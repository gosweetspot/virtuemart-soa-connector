<?php
/**
 * REDSOAP Component
 * 
 * @package    	com_vm_soa
 * @subpackage 	Components
 * @link		http://www.virtuemart-datamanager.com/
 * @author     	Mickael cabanas (admin|at|virtuemart-datamanager.com)
 * @copyright  	2012 Mickael Cabanas
 * @license		GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Hello Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
									 
class vm_soaControllervm_soa extends JControllerLegacy
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();

		// Register Extra tasks
		$this->registerTask( 'add'  , 	'edit' );
	}

	/**
	 * display the edit form
	 * @return void
	 */
	function edit()
	{
		
		/*JRequest::setVar( 'view', 'edit' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);*/

		parent::display();
	}

	/**
	 * save a record (and redirect to main page)
	 * @return void
	 */
	function save()
	{
		//$model = $this->getModel('config');
		
		$model = $this->getModel('vm_soa');
		$soapkey = JRequest::getVar('soapkey');
		;
		
		 
		 if ($model->saveSoapKey($soapkey) ) {
			$msg = JText::_( 'Key Saved !' );
		} else {
			$msg = JText::_( 'Error Saving Key' );
		}
		$link = 'index.php';
		$this->setRedirect($link, $msg);
		//$model->saveConf();
		//var_dump("save");die;
		//$ids = JRequest::getVar('cid', array(), '', 'array');
		//$values	= array('publish' => 1, 'unpublish' => 0);
		//$task	= $this->getTask();
		//$value	= JArrayHelper::getValue($values, $task, 0, 'int');
		
		
		//var_dump($task);die;
		
		/*if ($model->featured($ids,$value) ) {
			$msg = JText::_( 'Greeting Published/Unpublished!' );
		} else {
			$msg = JText::_( 'Error Saving Published' );
		}*/
		
		
		if ($model->store()) {
			$msg = JText::_( 'Greeting Saved!' );
		} else {
			$msg = JText::_( 'Error Saving Greeting' );
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$link = 'index.php?option=com_vm_soa';
		$this->setRedirect($link, $msg);
	}
	
	function apply()
	{
		$model = $this->getModel('vm_soa');
		$soapkey = JRequest::getVar('soapkey');
		;
		
		 
		 if ($model->saveSoapKey($soapkey) ) {
			$msg = JText::_( 'Key Saved !' );
		} else {
			$msg = JText::_( 'Error Saving Key' );
		}
		$link = 'index.php?option=com_vm_soa';
		$this->setRedirect($link, $msg);
		/*var_dump($soapkey);die;
		$model = $this->getModel('config');
		$model->saveConf();
		$ids = JRequest::getVar('cid', array(), '', 'array');
		$values	= array('publish' => 1, 'unpublish' => 0);
		$task	= $this->getTask();
		$value	= JArrayHelper::getValue($values, $task, 0, 'int');*/
		
		
		//var_dump($task);die;
		
		/*if ($model->featured($ids,$value) ) {
			$msg = JText::_( 'Greeting Published/Unpublished!' );
		} else {
			$msg = JText::_( 'Error Saving Published' );
		}*/
		
		
		/*if ($model->store()) {
			$msg = JText::_( 'Greeting Saved!' );
		} else {
			$msg = JText::_( 'Error Saving Greeting' );
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$link = 'index.php?option=com_vm_soa';
		$this->setRedirect($link, $msg);*/
	}
	
	function saveConf($soapkey)
	{
		
		//JRequest::checkToken() or jexit( 'Invalid Token (model config)' );
		
		
		if (!FSOAP) {
			$this->loadConf();
			$val = getValue($soapkey);
		}
		var_dump($val);die;
		/*if ($model->store($data)) {
			$msg = JText::_('COM_VIRTUEMART_CONFIG_SAVED');
			// Load the newly saved values into the session.
			VmConfig::loadConfig(true);
			
		}
		else {
			$msg = $model->getError();
			
		}*/

		$redir = 'index.php?option=com_virtuemart';
		if(JRequest::getCmd('task') == 'apply'){
			//$redir = $this->redirectPath;
		}
		
		
		//$this->setRedirect($redir, $msg);
	
	}
	
	function loadConf()
	{
		include('settings.php');		
	}

	/**
	 * remove record(s)
	 * @return void
	 */
	function remove()
	{
		$model = $this->getModel('vm_soa');
		if(!$model->delete()) {
			$msg = JText::_( 'Error: One or More Greetings Could not be Deleted' );
		} else {
			$msg = JText::_( 'Greeting(s) Deleted' );
		}

		$this->setRedirect( 'index.php?option=com_vm_soa', $msg );
	}
	
	/**
	 * remove record(s)
	 * @return void
	 */
	function publish()
	{
		//var_dump("publish");die;
		$model = $this->getModel('vm_soa');
		//var_dump($ids);die;
		$ids = JRequest::getVar('cid', array(), '', 'array');
		$values	= array('publish' => 1, 'unpublish' => 0);
		$task	= $this->getTask();
		$value	= JArrayHelper::getValue($values, $task, 0, 'int');
		
		
		//var_dump($value);die;
		
		if ($model->featured($ids,$value) ) {
			$msg = JText::_( 'Published !' );
		} else {
			$msg = JText::_( 'Error Saving Published' );
		}
		$link = 'index.php?option=com_vm_soa';
		$this->setRedirect($link, $msg);
	}
	
	/**
	 * remove record(s)
	 * @return void
	 */
	function unpublish()
	{
		
		$model = $this->getModel('vm_soa');
		//var_dump($ids);die;
		$ids = JRequest::getVar('cid', array(), '', 'array');
		$values	= array('publish' => 1, 'unpublish' => 0);
		$task	= $this->getTask();
		$value	= JArrayHelper::getValue($values, $task, 0, 'int');
		
		
		//var_dump($value);die;
		
		if ($model->featured($ids,$value) ) {
			$msg = JText::_( 'Unpublished!' );
		} else {
			$msg = JText::_( 'Error Saving Published' );
		}
		$link = 'index.php?option=com_vm_soa';
		$this->setRedirect($link, $msg);
	}
	
	

	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_vm_soa', $msg );
	}
}