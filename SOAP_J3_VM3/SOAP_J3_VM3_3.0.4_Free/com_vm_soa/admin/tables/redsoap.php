<?php
/**
 * REDSOAP Component
 * 
 * @package    	com_redsoap
 * @subpackage 	Components
 * @link		http://www.virtuemart-datamanager.com/
 * @author     	Mickael cabanas (admin|at|virtuemart-datamanager.com)
 * @copyright  	2012 Mickael Cabanas
 * @license		GNU/GPL
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Hello Table class
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class Tablevm_soa extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $id = null;

	/**
	 * @var string
	 */
	var $service = null;
	
	/**
	 * @var string
	 */
	var $value = null;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function TableRedsoap(& $db) {
		parent::__construct('#__vm_soap_config', 'id', $db);
	}
	
	function updateByService($service,$value) {
	
		if (!empty( $service )) {
			$query = " UPDATE #__vm_soap_config SET VALUE = ".$value.
					"  WHERE service = '".$service."' ";
			$this->_db->setQuery( $query );
			$this->_db->query();
			$err= $this->_db->getErrorMsg();
			return $err;
		}
	}
	
	function setAllDefaultValue($value) {
	
		if (isset( $value )) {
			$query = " UPDATE #__vm_soap_config SET VALUE = ".$value." ";
					
			$this->_db->setQuery( $query );
			$this->_db->query();
			$err= $this->_db->getErrorMsg();
			return $err;
		}
	}
	
	
	
}