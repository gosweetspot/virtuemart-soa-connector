<?php
/**
 * Classe de modèle spécifique aux clients
 *
 */
require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_soa'.DS.'conf.php';

class vm_soaModelconfig extends JModelLegacy
{
function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	/**
	 * Method to set the hello identifier
	 *
	 * @access	public
	 * @param	int Hello identifier
	 * @return	void
	 */
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a hello
	 * @return object with data
	 */
	function &getData()
	{
		// Load the data
		if (empty( $this->_data )) {
			$query = ' SELECT * FROM #__redsoap_config '.
					'  WHERE id = '.$this->_id;
			$this->_db->setQuery( $query );
			$this->_data = $this->_db->loadObject();
		}
		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->greeting = null;
		}
		return $this->_data;
	}

	/**
	 * Method to store a record
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function store()
	{	
		//$row =& $this->getTable();

		//$data = JRequest::get( 'post' );
	//	var_dump($data);die;
		/*var_dump($data);die;
		if ($data['storeunit']!=1){
			
			$this->updateValue($data);
			
			return true;
		}*/
		//var_dump($data);die;
		//$err= $row->updateByService('COM_REDSOAP_CATEGORYCACHE','0');
		//var_dump($err);die;
		
		// Bind the form fields to the hello table
	/*	if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Make sure the hello record is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the web link table to the database
		if (!$row->store()) {
			$this->setError( $row->getErrorMsg() );
			return false;
		}*/

		return true;
	}
	
	/**
	*
	**/
	function updateValue($data){
	
		$row =& $this->getTable();
		
		//set all to 0
		$err= $row->setAllDefaultValue('0');
		//var_dump($err);die;
		foreach ($data as $key => $value) {
		//	var_dump($key);var_dump($value);die;
			$err= $row->updateByService($key,$value);
		}

	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row =& $this->getTable();

		if (count( $cids )) {
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Method to toggle the featured setting of contacts.
	 *
	 * @param	array	$pks	The ids of the items to toggle.
	 * @param	int		$value	The value to toggle to.
	 *
	 * @return	boolean	True on success.
	 * @since	1.6
	 */
	public function featured($pks, $value = 0)
	{
	//	var_dump($pks);die;
		// Sanitize the ids.
		$pks = (array) $pks;
		JArrayHelper::toInteger($pks);

		if (empty($pks)) {
			$this->setError(JText::_('COM_CONTACT_NO_ITEM_SELECTED'));
			return false;
		}

		$table = $this->getTable();

		try
		{
			$db = $this->getDbo();

			$db->setQuery(
				'UPDATE #__redsoap_config' .
				' SET value = '.(int) $value.
				' WHERE id IN ('.implode(',', $pks).')'
			);
			if (!$db->query()) {
				throw new Exception($db->getErrorMsg());
			}

		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}

		$table->reorder();

		// Clean component's cache
		$this->cleanCache();

		return true;
	}
	/**
	 * Hellos data array
	 *
	 * @var array
	 */
	var $_data;


	/**
	 * Returns the query
	 * @return string The query to be used to retrieve the rows from the database
	 */
	function _buildQuery()
	{
		$query = ' SELECT * '
			. ' FROM #__vm_soap_config '
		;

		return $query;
	}

	/**
	 * Retrieves the hello data
	 * @return array Array of objects containing the data from the database
	 */
	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList( $query );
		}

		return $this->_data;
	}
	
	/**
	 * Retrieves the hello data
	 * @return array Array of objects containing the data from the database
	 */
	function getVersion()
	{
		$version ="";
		$db = JFactory::getDBO();
		
		$query  = "SELECT manifest_cache FROM `#__extensions` ext ";
		$query .= "WHERE element ='com_vm_soa'  ";
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		foreach ($rows as $row)	{

			$data = explode(',', $row->manifest_cache);
			$count = count($data);
			for ($i = 0; $i < $count; $i++) {
				//echo "data ".$data[$i];
				$data2 = explode(':',$data[$i]);
				//echo "data :".$data2[0].' -> '.$data2[1].'\n';
				if ($data2[0]=='"version"'){
				
					$version = $data2[1];
				}
			}
		
		}
		$version = str_replace('"', '', $version);
		
		return $version;
	}
	
	function saveConf()
	{
		
		JRequest::checkToken() or jexit( 'Invalid Token (model config)' );
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'tables');
		
		if(!class_exists('VmConfig'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');
		if(!class_exists('VirtueMartModelConfig'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'models'.DS.'config.php');
		$model = new VirtueMartModelConfig();

		$data = JRequest::get('post');
		
		$data['offline_message'] = JRequest::getVar('offline_message','','post','STRING',JREQUEST_ALLOWHTML);

		if(strpos($data['offline_message'],'|')!==false){
			$data['offline_message'] = str_replace('|','',$data['offline_message']);
		}
		if (!FSOAP) {
			$this->loadConf();
			$val = getValue();
			$data = array_merge($data,$val);
		}
		//var_dump($data);die;
		if ($model->store($data)) {
			$msg = JText::_('COM_VIRTUEMART_CONFIG_SAVED');
			// Load the newly saved values into the session.
			VmConfig::loadConfig(true);
			
		}
		else {
			$msg = $model->getError();
			
		}

		$redir = 'index.php?option=com_virtuemart';
		if(JRequest::getCmd('task') == 'apply'){
			//$redir = $this->redirectPath;
		}
		
		
		//$this->setRedirect($redir, $msg);
	
	}
	
	function loadConf()
	{
		include('settings.php');		
	}
	
	
	function renewConfig() {
		//JUtility::getToken() has been removed. Use JSession::getFormToken() instead.
		$token  = JSession::getFormToken();
		$_REQUEST[$token] = $token;
		$_POST[$token] = $token;
		
		$data = $this->readConfigFile("");
		
		if(!class_exists('VirtueMartModelConfig'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'models'.DS.'config.php');
		$model = new VirtueMartModelConfig();

		if ($model->store($data)) {
			$msg = JText::_('COM_VIRTUEMART_CONFIG_SAVED');
			// Load the newly saved values into the session.
			VmConfig::loadConfig(true);
		}
	
	}
	
	function readConfigFile($returnDangerousTools){

		define('JPATH_SOA_ADMINISTRATOR' , JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_soa');
		$_datafile = JPATH_SOA_ADMINISTRATOR.DS.'soap.cfg';
		if (!file_exists($_datafile)) {
			if (file_exists(JPATH_SOA_ADMINISTRATOR.DS.'com_vm_soa_defaults.cfg-dist')) {
				if(!class_exists('JFile')) require(JPATH_VM_LIBRARIES.DS.'joomla'.DS.'filesystem'.DS.'file.php');
				JFile::copy('com_vm_soa_defaults.cfg-dist','soap.cfg',JPATH_SOA_ADMINISTRATOR);
			} else {
				JError::raiseWarning(500, 'The data file with the default configuration could not be found. You must configure the shop manually.');
				return false;
			}

		} else {
			vmInfo('Taking config from file');
		}

		$_section = '[CONFIG]';
		$_data = fopen($_datafile, 'r');
		$_configData = array();
		$_switch = false;
		while ($_line = fgets ($_data)) {
			$_line = trim($_line);

			if (strpos($_line, '#') === 0) {
				continue; // Commentline
			}
			if ($_line == '') {
				continue; // Empty line
			}
			if (strpos($_line, '[') === 0) {
				// New section, check if it's what we want
				if (strtoupper($_line) == $_section) {
					$_switch = true; // Ok, right section
				} else {
					$_switch = false;
				}
				continue;
			}
			if (!$_switch) {
				continue; // Outside a section or inside the wrong one.
			}

			if (strpos($_line, '=') !== false) {

				$pair = explode('=',$_line);
				$dataConf[$pair[0]] = $pair[1];
				
				/*if(isset($pair[1])){
					if(strpos($pair[1], 'array:') !== false){
						$pair[1] = substr($pair[1],6);
						$pair[1] = explode('|',$pair[1]);
					}
					// if($pair[0]!=='offline_message' && $pair[0]!=='dateformat'){
					if($pair[0]!=='offline_message'){
						$_line = $pair[0].'='.serialize($pair[1]);
					} else {
						$_line = $pair[0].'='.base64_encode(serialize($pair[1]));
					}

					if($returnDangerousTools && $pair[0] == 'dangeroustools' ){
						vmdebug('dangeroustools'.$pair[1]);
						if($pair[1]=="0") return false; else return true;
					}

				} else {
					$_line = $pair[0].'=';
				}*/
				$_configData[] = $_line;

			}

		}

		fclose ($_data);
		//var_dump($dataConf);die;
		
		if (!$dataConf) {
			return false; // Nothing to do
		} else {
			return $dataConf;
		}
		
		/*if (!$_configData) {
			return false; // Nothing to do
		} else {
			return $_configData;
		}*/
	}
}
?>