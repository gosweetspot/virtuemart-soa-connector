<?php
/**
 * @package    	com_vm_soa (WebServices for virtuemart)
 * @author		Mickael Cabanas (cabanas.mickael|at|gmail.com)
 * @link 		http://www.virtuemart-datamanager.com
 * @license    	GNU/GPL
*/
jimport( 'joomla.application.component.model' );
class vm_soaModelvm_soa extends JModelLegacy
{
		function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	/**
	 * Method to set the hello identifier
	 *
	 * @access	public
	 * @param	int Hello identifier
	 * @return	void
	 */
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a hello
	 * @return object with data
	 */
	function &getData()
	{
		// Load the data
		if (empty( $this->_data )) {
			$query = ' SELECT * FROM #__vm_soap_config '.
					'  WHERE id = '.$this->_id;
			$this->_db->setQuery( $query );
			$this->_data = $this->_db->loadObject();
		}
		if (!$this->_data) {
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->greeting = null;
		}
		return $this->_data;
	}

	/**
	 * Method to store a record
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function store()
	{	
		$row =& $this->getTable();

		$data = JRequest::get( 'post' );
		/*var_dump($data);die;
		if ($data['storeunit']!=1){
			
			$this->updateValue($data);
			
			return true;
		}*/
		//var_dump($data);die;
		//$err= $row->updateByService('COM_REDSOAP_CATEGORYCACHE','0');
		//var_dump($err);die;
		
		// Bind the form fields to the hello table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Make sure the hello record is valid
		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the web link table to the database
		if (!$row->store()) {
			$this->setError( $row->getErrorMsg() );
			return false;
		}

		return true;
	}
	
	/**
	*
	**/
	function updateValue($data){
	
		$row =& $this->getTable();
		
		//set all to 0
		$err= $row->setAllDefaultValue('0');
		//var_dump($err);die;
		foreach ($data as $key => $value) {
		//	var_dump($key);var_dump($value);die;
			$err= $row->updateByService($key,$value);
		}

	}

	/**
	 * Method to delete record(s)
	 *
	 * @access	public
	 * @return	boolean	True on success
	 */
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row =& $this->getTable();

		if (count( $cids )) {
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Method to toggle the featured setting of contacts.
	 *
	 * @param	array	$pks	The ids of the items to toggle.
	 * @param	int		$value	The value to toggle to.
	 *
	 * @return	boolean	True on success.
	 * @since	1.6
	 */
	public function featured($pks, $value = 0)
	{
	
	//	var_dump($pks);die;
		// Sanitize the ids.
		$pks = (array) $pks;
		JArrayHelper::toInteger($pks);

		if (empty($pks)) {
		
			$this->setError(JText::_('COM_CONTACT_NO_ITEM_SELECTED'));
			return false;
		}

		$table = $this->getTable();
//var_dump("table");die;
		try
		{
			$db = $this->getDbo();

			$db->setQuery(
				'UPDATE #__vm_soap_config' .
				' SET value = '.(int) $value.
				' WHERE id IN ('.implode(',', $pks).')'
			);
			if (!$db->query()) {
				throw new Exception($db->getErrorMsg());
			}

		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}

		//$table->reorder();

		// Clean component's cache
		$this->cleanCache();

		return true;
	}
	
	public function saveSoapKey($soapkey)
	{
	

		if (empty($soapkey)) {
		
			$this->setError(JText::_('COM_CONTACT_NO_ITEM_SELECTED'));
			return false;
		}

		$table = $this->getTable();

		try
		{
			$db = $this->getDbo();

			$db->setQuery(
				"UPDATE #__vm_soap_config SET conf = '".$soapkey."' WHERE service = 'SOAPKEY'"
			);
			if (!$db->query()) {
				throw new Exception($db->getErrorMsg());
			}

		}
		catch (Exception $e)
		{
			//var_dump($e);die;
			$this->setError($e->getMessage());
			return false;
		}

		//$table->reorder();

		// Clean component's cache
		$this->cleanCache();

		return true;
	}
	

}
?>