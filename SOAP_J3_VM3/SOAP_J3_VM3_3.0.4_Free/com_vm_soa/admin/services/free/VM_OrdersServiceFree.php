<?php

define( '_VALID_MOS', 1 );
define( '_JEXEC', 1 );

/**
 * Virtuemart Order SOA Connector
 *
 * Virtuemart Order SOA Connector (Provide functions getOrdersFromStatus, getOrderStatus, getOrder, getAllOrders)
 *
 * @package    com_vm_soa
 * @subpackage component
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  2012 Mickael Cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    $Id:$
 * 
 * GSS
 *
 * NOTICE OF LICENSE
 *
 * This file is part of Order Integration.
 *
 * Virtuemart Order SOA Connector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * Virtuemart Order SOA Connector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with Virtuemart Order SOA Connector.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/** loading framework **/
include_once('../VM_Commons.php');

/**
 * Class OrderStatus
 *
 * Class "OrderStatus" with attribute : id, name, code,
 *
 *
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class OrderStatus {

	public $order_status_id="";
	public $order_status_code="";
	public $order_status_name="";
	public $order_status_description="";
	public $ordering="";
	public $vendor_id="";
	public $published="";


	function __construct($order_status_id,$order_status_code,$order_status_name,$order_status_description,$ordering,$vendor_id,$published){

		$this->order_status_id			=$order_status_id;
		$this->order_status_code		=$order_status_code;
		$this->order_status_name		=$order_status_name;
		$this->order_status_description	=$order_status_description;
		$this->ordering					=$ordering;
		$this->vendor_id				=$vendor_id;
		$this->published				=$published;

	}

}

/**
 * Class Order
 *
 * Class "Order" with attribute : id, user_id, vendor_id,  order_number, user_info_id , order_total order_subtotal
 * order_tax, order_tax_details order_shipment, coupon_discount order_currency ...)
 *
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   06/04/2016: Add new properties
 */
class Order {
	public $id="";
	public $user_id="";
	public $vendor_id="";
	public $order_number="";
	public $order_pass="";
	public $user_info_id="";
	public $order_total="";
	public $order_subtotal="";
	public $order_tax="";
	public $order_tax_details="";
	public $order_shipment="";
	public $order_shipment_tax="";
	public $order_payment="";
	public $order_payment_tax="";
	public $coupon_discount="";
	public $coupon_code="";
	public $order_discount="";
	public $order_currency="";
	public $order_status="";
	public $user_currency_id="";
	public $user_currency_rate="";
	public $virtuemart_paymentmethod_id="";
	public $virtuemart_shipmentmethod_id="";
	public $customer_note="";
	public $ip_address="";
	public $created_on="";
	public $modified_on="";
	public $shipping_address = null;
	public $products = null;
		
	//constructeur
	function __construct($id,$user_id,$vendor_id,$order_number,$order_pass,$user_info_id,$order_total,$order_subtotal,$order_tax,$order_tax_details,$order_shipment,$order_shipment_tax,$order_payment,$order_payment_tax,
			$coupon_discount,$coupon_code,$order_discount,$order_currency,$order_status,$user_currency_id,$user_currency_rate,$virtuemart_paymentmethod_id,$virtuemart_shipmentmethod_id,$customer_note,$ip_address,$created_on,$modified_on, $shipping_address, $products) {

				$this->id					=$id;
				$this->user_id				=$user_id;
				$this->vendor_id			=$vendor_id;
				$this->order_number			=$order_number;
				$this->order_pass			=$order_pass;
				$this->user_info_id			=$user_info_id;
				$this->order_total			=$order_total;
				$this->order_subtotal		=$order_subtotal;
				$this->order_tax			=$order_tax;
				$this->order_tax_details	=$order_tax_details;
				$this->order_shipment		=$order_shipment;
				$this->order_shipment_tax	=$order_shipment_tax;
				$this->order_payment		=$order_payment;
				$this->order_payment_tax	=$order_payment_tax;
				$this->coupon_discount		=$coupon_discount;
				$this->coupon_code			=$coupon_code;
				$this->order_discount		=$order_discount;
				$this->order_currency		=$order_currency;
				$this->order_status			=$order_status;
				$this->user_currency_id		=$user_currency_id;
				$this->user_currency_rate	=$user_currency_rate;
				$this->virtuemart_paymentmethod_id	=$virtuemart_paymentmethod_id;
				$this->virtuemart_shipmentmethod_id		=$virtuemart_shipmentmethod_id;
				$this->customer_note		=$customer_note;
				$this->ip_address			=$ip_address;
				$this->created_on			=$created_on;
				$this->modified_on			=$modified_on;
				$this->shipping_address		=$shipping_address;
				$this->products				=$products;
	}
}

/**
 * Class User
 *
 * Class "User" with attribute : id, name, code,
 *
 *
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   06/04/2016: Add new properties
 */
class User {

	public $user_id="";
	public $email="";
	public $username="";
	public $password="";
	public $userinfo_id="";
	public $address_type="";
	public $address_type_name="";
	public $name="";
	public $company="";
	public $title="";
	public $last_name="";
	public $first_name="";
	public $middle_name="";
	public $phone_1="";
	public $phone_2="";
	public $fax="";
	public $address_1="";
	public $address_2="";
	public $city="";
	public $virtuemart_state_id="";
	public $virtuemart_country_id="";
	public $zip="";
	public $extra_field_1="";
	public $extra_field_2="";
	public $extra_field_3="";
	public $extra_field_4="";
	public $extra_field_5="";
	public $created_on="";
	public $modified_on="";
	public $user_is_vendor="";
	public $customer_number="";
	public $perms="";
	public $virtuemart_paymentmethod_id="";
	public $virtuemart_shippingcarrier_id="";
	public $agreed="";
	public $shoppergroup_id="";
	public $extra_fields_data="";
	public $state_name = "";
	public $country_name = "";
	public $country_code = "";

	function __construct($user_id,$email,$username,$password,$userinfo_id,$address_type,$address_type_name,$name,$company,$title,$last_name,
			$first_name,$middle_name,$phone_1,$phone_2,$fax,$address_1,$address_2,$city,$virtuemart_state_id,$virtuemart_country_id,$zip,
			$extra_field_1,$extra_field_2,$extra_field_3,$extra_field_4,$extra_field_5,$created_on,$modified_on
			,$user_is_vendor,$customer_number,$perms,$virtuemart_paymentmethod_id,$virtuemart_shippingcarrier_id,$agreed,$shoppergroup_id,$extra_fields_data,$state_name,$country_name,$country_code){

				$this->user_id					=$user_id;
				$this->email					=$email;
				$this->username					=$username;
				$this->password					=$password;
				$this->userinfo_id				=$userinfo_id;
				$this->address_type				=$address_type;
				$this->address_type_name		=$address_type_name;
				$this->name						=$name;
				$this->company					=$company;
				$this->title					=$title;
				$this->last_name				=$last_name;
				$this->first_name				=$first_name;
				$this->middle_name				=$middle_name;
				$this->phone_1					=$phone_1;
				$this->phone_2					=$phone_2;
				$this->fax						=$fax;
				$this->address_1				=$address_1;
				$this->address_2				=$address_2;
				$this->city						=$city;
				$this->virtuemart_state_id		=$virtuemart_state_id;
				$this->virtuemart_country_id	=$virtuemart_country_id;
				$this->zip						=$zip;
				$this->extra_field_1			=$extra_field_1;
				$this->extra_field_2			=$extra_field_2;
				$this->extra_field_3			=$extra_field_3;
				$this->extra_field_4			=$extra_field_4;
				$this->extra_field_5			=$extra_field_5;
				$this->created_on				=$created_on;
				$this->modified_on				=$modified_on;
				$this->user_is_vendor			=$user_is_vendor;
				$this->customer_number			=$customer_number;
				$this->perms					=$perms;
				$this->virtuemart_paymentmethod_id		=$virtuemart_paymentmethod_id;
				$this->virtuemart_shippingcarrier_id 	=$virtuemart_shippingcarrier_id;
				$this->agreed					=$agreed;
				$this->shoppergroup_id			=$shoppergroup_id;
				$this->extra_fields_data		=$extra_fields_data;
				$this->state_name				=$state_name;
				$this->country_name				=$country_name;
				$this->country_code				=$country_code;
	}

}

/**
 * Class OrderItemInfo
 *
 * Class "OrderItemInfo" with attribute : id, name, description, price, quantity, image, fulliamage ,
 * attributes, parent Product, child id)
 *
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class OrderItemInfo {

	public $order_id="";
	public $userinfo_id="";
	public $vendor_id="";
	public $product_id="";
	public $order_item_sku="";
	public $order_item_name="";
	public $product_quantity="";
	public $product_item_price="";
	public $product_final_price="";
	public $order_item_currency="";
	public $order_status="";
	public $product_attribute;
	public $created_on="";
	public $modified_on="";


	//constructeur
	/**
	 * Enter description here...
	 *
	 * @param String $name
	 * @param String $price
	 * @param String $description
	 * @param String $image
	 * @param String $fullimage
	 * @param String $id
	 */
	function __construct($order_id, $userinfo_id, $vendor_id, $product_id, $order_item_sku, $order_item_name, $product_quantity, $product_item_price, $product_final_price,$order_item_currency,$order_status,$product_attribute,$created_on,$modified_on) {

		$this->order_id = $order_id;
		$this->userinfo_id = $userinfo_id;
		$this->vendor_id = $vendor_id;
		$this->product_id = $product_id;
		$this->order_item_sku = $order_item_sku;
		$this->order_item_name = $order_item_name;
		$this->product_quantity = $product_quantity;
		$this->product_item_price = $product_item_price;
		$this->product_final_price = $product_final_price;
		$this->order_item_currency = $order_item_currency;
		$this->order_status = $order_status;
		$this->product_attribute = $product_attribute;
		$this->created_on = $created_on;
		$this->modified_on = $modified_on;


	}
}

/**
 * Class ShippingRate
 *
 * Class "ShippingRate" with attribute : shipping_rate_id ...,
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class ShippingRate { //NOT IN VM2
	public $shipping_rate_id="";
	public $shipping_rate_name="";
	public $shipping_rate_carrier_id="";
	public $shipping_rate_country="";
	public $shipping_rate_zip_start="";
	public $shipping_rate_zip_end="";
	public $shipping_rate_weight_start="";
	public $shipping_rate_weight_end="";
	public $shipping_rate_value="";
	public $shipping_rate_package_fee="";
	public $shipping_rate_currency_id="";
	public $shipping_rate_vat_id="";
	public $shipping_rate_list_order="";
		
	//constructeur
	function __construct($shipping_rate_id,$shipping_rate_name,$shipping_rate_carrier_id,$shipping_rate_country,$shipping_rate_zip_start,
			$shipping_rate_zip_end,$shipping_rate_weight_start,$shipping_rate_weight_end,$shipping_rate_value,$shipping_rate_package_fee,$shipping_rate_currency_id,
			$shipping_rate_vat_id,$shipping_rate_list_order) {

				$this->shipping_rate_id=$shipping_rate_id;
				$this->shipping_rate_name=$shipping_rate_name;
				$this->shipping_rate_carrier_id=$shipping_rate_carrier_id;
				$this->shipping_rate_country=$shipping_rate_country;
				$this->shipping_rate_zip_start=$shipping_rate_zip_start;
				$this->shipping_rate_zip_end=$shipping_rate_zip_end;
				$this->shipping_rate_weight_start=$shipping_rate_weight_start;
				$this->shipping_rate_weight_end=$shipping_rate_weight_end;
				$this->shipping_rate_value=$shipping_rate_value;
				$this->shipping_rate_package_fee=$shipping_rate_package_fee;
				$this->shipping_rate_currency_id=$shipping_rate_currency_id;
				$this->shipping_rate_vat_id=$shipping_rate_vat_id;
				$this->shipping_rate_list_order=$shipping_rate_list_order;
					
	}
}

/**
 * Class Coupon
 *
 * Class "Coupon" with attribute : coupon_id ...
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class Coupon {

	public $coupon_id="";
	public $coupon_code="";
	public $percent_or_total="";
	public $coupon_type="";
	public $coupon_value="";
	public $coupon_start_date="";
	public $coupon_expiry_date="";
	public $coupon_value_valid="";
	public $published="";

	function __construct($coupon_id, $coupon_code,$percent_or_total,$coupon_type,$coupon_value,$coupon_start_date,$coupon_expiry_date,$coupon_value_valid,$published){

		$this->coupon_id=$coupon_id;
		$this->coupon_code=$coupon_code;
		$this->percent_or_total=$percent_or_total;
		$this->coupon_type=$coupon_type;
		$this->coupon_value=$coupon_value;
		$this->coupon_start_date=$coupon_start_date;
		$this->coupon_expiry_date=$coupon_expiry_date;
		$this->coupon_value_valid=$coupon_value_valid;
		$this->published=$published;
			
	}

}

/**
 * Class ShippingMethod
 *
 * Class "ShippingMethod" with attribute : shipping_carrier_id ...
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class ShippingMethod {

	public $virtuemart_shipmentmethod_id="";
	public $virtuemart_vendor_id="";
	public $shipment_jplugin_id ="";
	public $slug="";
	public $shipment_element="";
	public $shipment_params="";
	public $shipment_name="";
	public $shipment_desc="";
	public $virtuemart_shoppergroup_id="";
	public $ordering="";
	public $shared="";
	public $published="";

	function __construct($virtuemart_shipmentmethod_id,$virtuemart_vendor_id,$shipment_jplugin_id ,$slug,$shipment_element
			,$shipment_params,$shipment_name,$shipment_desc,$virtuemart_shoppergroup_id,$ordering,$shared,$published){

				$this->virtuemart_shipmentmethod_id=$virtuemart_shipmentmethod_id;
				$this->virtuemart_vendor_id=$virtuemart_vendor_id;
				$this->shipment_jplugin_id 	=$shipment_jplugin_id ;
				$this->slug=$slug;
				$this->shipment_element=$shipment_element;
				$this->shipment_params=$shipment_params;
				$this->shipment_name=$shipment_name;
				$this->shipment_desc=$shipment_desc;
				$this->virtuemart_shoppergroup_id=$virtuemart_shoppergroup_id;
				$this->ordering=$ordering;
				$this->shared=$shared;
				$this->published=$published;
					
	}
}

/**
 * Class PaymentMethod
 *
 * Class "PaymentMethod" with attribute : payment_method_id ...,
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class PaymentMethod {
	public $payment_method_id="";
	public $virtuemart_vendor_id="";
	public $payment_jplugin_id="";
	public $payment_name="";
	public $payment_element="";
	public $discount="";
	public $discount_is_percentage="";
	public $discount_max_amount="";
	public $discount_min_amount="";
	public $payment_params="";
	public $shared="";
	public $ordering="";
	public $published="";
	public $payment_enabled="";
	public $accepted_creditcards="";
	public $payment_extrainfo="";

	//constructeur
	function __construct($payment_method_id,$virtuemart_vendor_id,$payment_jplugin_id,$payment_name,$payment_element,
			$discount,$discount_is_percentage,$discount_max_amount,$discount_min_amount,$payment_params,$shared,$ordering,$published) {

				$this->payment_method_id=$payment_method_id;
				$this->virtuemart_vendor_id=$virtuemart_vendor_id;
				$this->payment_jplugin_id=$payment_jplugin_id	;
				$this->payment_name=$payment_name;
				$this->payment_element=$payment_element;
				$this->discount=$discount;
				$this->discount_is_percentage=$discount_is_percentage;
				$this->discount_max_amount=$discount_max_amount;
				$this->discount_min_amount=$discount_min_amount;
				$this->payment_params=$payment_params;
				$this->shared=$shared;
				$this->ordering=$ordering;
				$this->published=$published;
					
					
	}
}

/**
 * Class Creditcard
 *
 * Class "Creditcard" with attribute : creditcard_id ...
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class Creditcard {

	public $creditcard_id="";
	public $vendor_id="";
	public $creditcard_name="";
	public $creditcard_code="";
	public $shared="";
	public $published="";

	function __construct($creditcard_id,$vendor_id,$creditcard_name,$creditcard_code,$shared,$published){

		$this->creditcard_id	=$creditcard_id;
		$this->vendor_id		=$vendor_id;
		$this->creditcard_name	=$creditcard_name;
		$this->creditcard_code	=$creditcard_code;
		$this->shared			=$shared;
		$this->published		=$published;
	}
}

/**
 * Class Plugin
 *
 * Class "Plugin" with attribute : extension_id ...
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class Plugin {

	public $extension_id="";
	public $name="";
	public $type="";
	public $element="";
	public $folder="";
	public $client_id="";
	public $enabled="";
	public $access="";
	public $protected="";
	public $manifest_cache="";
	public $params="";
	public $custom_data="";
	public $system_data="";
	public $ordering="";
	public $state="";

	function __construct($extension_id, $name,$type,$element,$folder,$client_id,$enabled,$access,$protected,$manifest_cache
			,$params,$custom_data,$system_data,$ordering,$state){

				$this->extension_id=$extension_id;
				$this->name=$name;
				$this->type=$type;
				$this->element=$element;
				$this->folder=$folder;
				$this->client_id=$client_id;
				$this->enabled=$enabled;
				$this->access=$access;
				$this->protected=$protected;
				$this->manifest_cache=$manifest_cache;
				$this->params=$params;
				$this->custom_data=$custom_data;
				$this->system_data=$system_data;
				$this->ordering=$ordering;
				$this->state=$state;
					
	}

}
/**
 * Class CommonReturn
 *
 * Class "CommonReturn" with attribute : returnCode, message, code,
 *
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 */
class CommonReturn {
	public $returnCode="";
	public $message="";
	public $returnData="";

	//constructeur
	/**
	 * Enter description here...
	 *
	 * @param String $returnCode
	 * @param String $message
	 */
	function __construct($returnCode, $message, $returnData) {
		$this->returnCode = $returnCode;
		$this->message = $message;
		$this->returnData = $returnData;
	}
}



/**
 * This function getOrderStatus return all status avalaible
 * (expose as WS)
 * @param
 * @return array of Status
 */
function getOrderStatus($params) {

	/* Authenticate*/
	$result = onAdminAuthenticate($params->login, $params->password, $params->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		if (!class_exists( 'VirtueMartModelOrderstatus' )) require (JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orderstatus.php');
		$VirtueMartModelOrderstatus = new VirtueMartModelOrderstatus;
			
		$listStatus = $VirtueMartModelOrderstatus->getOrderStatusList();
			
		foreach ($listStatus as $status)
		{
			$OrderStatus = new OrderStatus($status->virtuemart_orderstate_id,
					$status->order_status_code,
					$status->order_status_name,
					$status->order_status_description,
					$status->ordering,
					$status->virtuemart_vendor_id,
					$status->published);
			$arrayStatus[]= $OrderStatus;
		}
		return $arrayStatus;
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->login);
	}
}


/**
 * This is generic function to get order details
 * (NOT expose as WS)
 * @param Object
 * @return order details
 * 
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   06/04/2016: Add new properties
 */
function getOrderGeneric($params) {
		
	if (empty($params->limite_start)){
		$params->limite_start="0";
	}
	if (empty($params->limite_end)){
		$params->limite_end="300";
	}
		
	$db = JFactory::getDBO();
	$query  = "SELECT * FROM `#__virtuemart_orders` WHERE 1 ";
		
	if (!empty($params->status)){
		$query .= " AND order_status = '$params->status' ";
	}
	if (!empty($params->order_id)){
		$query .= " AND virtuemart_order_id = '$params->order_id' ";
	}
	if (!empty($params->order_number)){
		$query .= " AND order_number = '$params->order_number' ";
	}
		
	//format date en entree : '2011-07-25 00:00:00' ou '2011-07-18'
	if (!empty($params->date_start)){
		$query .= " AND created_on BETWEEN '$params->date_start' AND '$params->date_end' ";
	}
		
	$query .= " ORDER BY virtuemart_order_id desc ";
	$query .= " LIMIT $params->limite_start, $params->limite_end ";
		
	$db->setQuery($query);
		
	$rows = $db->loadObjectList();
	//return new SoapFault("TEST", "TEST VM_ID: ".$query."\n".$row->created_on."\n".$date_end);
	foreach ($rows as $row){

		$arr_address = GetAdditionalUserInfo($row->virtuemart_order_id);
		$shipping_address = MapMissingFields($arr_address);
		
		$arr_products = GetProductsFromOrderId($row->virtuemart_order_id);
		$Order = new Order($row->virtuemart_order_id,
				$row->virtuemart_user_id,
				$row->virtuemart_vendor_id,
				$row->order_number,
				$row->order_pass,
				$row->virtuemart_userinfo_id ,
				$row->order_total,
				$row->order_subtotal,
				$row->order_tax,
				$row->order_tax_details,
				$row->order_shipment,
				$row->order_shipment_tax,
				$row->order_payment,
				$row->order_payment_tax,
				$row->coupon_discount,
				$row->coupon_code,
				$row->order_discount,
				$row->order_currency,
				$row->order_status,
				$row->user_currency_id,
				$row->user_currency_rate,
				$row->virtuemart_paymentmethod_id,
				$row->virtuemart_shipmentmethod_id,
				$row->customer_note,
				$row->ip_address,
				$row->created_on,
				$row->modified_on,
				$shipping_address,
				$arr_products
				);
		$orderArray[]=$Order;
			
	}
	return $orderArray;
}

/**
 * This function Map the missing fields in the address
 * (expose as WS)
 * @param
 * @return result
 *
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Ezequiel Reyno
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   12/05/2016: MapMissingFields
 */
function MapMissingFields($arr_address) {
	$billing_address = null;
	$shipping_address = null;
	foreach ( $arr_address as $element ) {
		if ( "ST" == $element->address_type) {
			$shipping_address = $element;
		}
		if ( "BT" == $element->address_type) {
			$billing_address = $element;
		}
	}
	if(!isset($shipping_address)){
		$shipping_address = $billing_address;
	}
	if(!isset($shipping_address->first_name))
		$shipping_address->first_name = $billing_address->first_name;
	if(!isset($shipping_address->last_name))
		$shipping_address->last_name = $billing_address->last_name;
	if(!isset($shipping_address->address_1))
		$shipping_address->address_1 = $billing_address->address_1;
	if(!isset($shipping_address->country_code))
		$shipping_address->country_code = $billing_address->country_code;
	if(!isset($shipping_address->city))
		$shipping_address->city = $billing_address->city;
	if(!isset($shipping_address->email))
		$shipping_address->email = $billing_address->email;
	if(!isset($shipping_address->phone_1))
		$shipping_address->phone_1 = $billing_address->phone_1;
	return $shipping_address;
}

/**
 * This function to Get Additional User Info
 * (expose as WS)
 * @param
 * @return result
 * 
 * @author     Mickael cabanas (cabanas.mickael|at|gmail.com)
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Mickael cabanas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   06/04/2016: Add new properties
 */
function GetAdditionalUserInfo($order_id) {

	$db = JFactory::getDBO();

		
	$query   = "SELECT * FROM `#__virtuemart_order_userinfos` ui ";
	//$query  .= "LEFT JOIN `#__users` u ON ui.virtuemart_user_id=u.id ";
	$query  .= "LEFT JOIN `#__virtuemart_countries` c ON ui.virtuemart_country_id=c.virtuemart_country_id WHERE";
	$query .= " ui.virtuemart_order_id = " .$order_id;
	
	/*
	if (!empty($user_id)){
		$query .= " AND ui.virtuemart_user_id = '$user_id'";
	}*/
	$query .= " LIMIT 0,100 ";
		

	$db->setQuery($query);
		
	$rows = $db->loadObjectList();
		
	foreach ($rows as $row){

		$User = new User($row->virtuemart_user_id,
				$row->email,
				$row->username,
				"*******",
				$row->virtuemart_order_userinfo_id,
				$row->address_type,
				$row->address_type_name,
				$row->name,
				$row->company,
				$row->title,
				$row->last_name,
				$row->first_name,
				$row->middle_name,
				$row->phone_1,
				$row->phone_2,
				$row->fax,
				$row->address_1,
				$row->address_2,
				$row->city,
				$row->virtuemart_state_id,
				$row->virtuemart_country_id,
				$row->zip ,
				$row->extra_field_1,
				$row->extra_field_2,
				$row->extra_field_3,
				$row->extra_field_4,
				$row->extra_field_5,
				$row->created_on,
				$row->modified_on,
				$row->user_is_vendor,
				$row->customer_number,
				$row->perms,
				$row->virtuemart_paymentmethod_id,
				$row->virtuemart_shippingcarrier_id,
				$row->agreed,
				$row->virtuemart_shoppergroup_id,
				"",
				"",//$row->state_name,
				$row->country_name,
				$row->country_2_code
				);
		$arrayUser[]= $User;

	}
		
	$errMsg=  $db->getErrorMsg();
	if ($errMsg==null){
		return $arrayUser;
	}else {
		return new SoapFault("GetAdditionalUserInfoFault", "Error in GetAdditionalUserInfo ",$errMsg);
	}
}

/**
 * This function get Products for a order ID
 * (expose as WS)
 * @param string
 * @return array (Product)
 * 
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Ezequiel Reyno
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 
 */
function GetProductsFromOrderId($order_id) {

	$db = JFactory::getDBO();
	$query  = "SELECT *  FROM #__virtuemart_order_items WHERE virtuemart_order_id = '$order_id'  ";

	$db->setQuery($query);

	$rows = $db->loadObjectList();

	foreach ($rows as $row){
			
		$OrderItemInfo = new OrderItemInfo($row->virtuemart_order_id,
				$row->virtuemart_userinfo_id,
				$row->virtuemart_vendor_id,
				$row->virtuemart_product_id,
				$row->order_item_sku,
				$row->order_item_name,
				$row->product_quantity,
				$row->product_item_price,
				$row->product_final_price,
				$row->order_item_currency,
				$row->order_status,
				$row->product_attribute,
				$row->created_on,
				$row->modified_on

				);
			
		$OrderItemInfoArray[] =  $OrderItemInfo;

	}


	return $OrderItemInfoArray;
}

/**
 * This function get order details from order id
 * (expose as WS)
 * @param string The id of the order
 * @return order details
 */
function getOrder($params) {

	$order_id=$params->order_id;
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

			
		$ord = getOrderGeneric($params);
		return $ord[0];
		/*if (!class_exists( 'VirtueMartModelOrders' )) require (JPATH_VM_ADMINISTRATOR.DS.'models'.DS.'orders.php');
			$VirtueMartModelOrders = new VirtueMartModelOrders;
				
			$orderInfo = $VirtueMartModelOrders->getOrder($order_id);
			$orderInfo['details'];
			$orderInfo['history'];
			$orderInfo['items'];
				
			//TODO : A terminer
			$Order = new Order($orderInfo['details']->order_id,$orderInfo['details']->user_id,$orderInfo['details']->vendor_id, $orderInfo['details']->order_number, $orderInfo['details']->user_info_id, $orderInfo['details']->order_total, $orderInfo['details']->order_subtotal,
			$orderInfo['details']->order_tax, $orderInfo['details']->order_tax_details, $orderInfo['details']->order_shipping, $orderInfo['details']->order_shipping_tax, $orderInfo['details']->coupon_discount, $orderInfo['details']->coupon_code, $orderInfo['details']->order_discount, $orderInfo['details']->order_currency,
			$orderInfo['details']->order_status, $orderInfo['details']->cdate, $orderInfo['details']->mdate, $orderInfo['details']->ship_method_id, $orderInfo['details']->customer_note, $orderInfo['details']->ip_address);
				
			return $Order;*/
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}

/**
 * This function get all orders with specified status P, C, R etc...
 * (expose as WS)
 * @param string params (limiteStart, LimitEnd, Status)
 * @return array of orders
 */
function getOrdersFromStatus($params) {

	/* Authenticate*/

	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}
	//Auth OK
	if ($result == "true"){

		if (empty($params->limite_start)){
			$params->limite_start="0";
		}
		if (empty($params->limite_end)){
			$params->limite_end="300";
		}
			
		return getOrderGeneric($params);
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}

/**
 * This function update the status of an particular order adding a coment.
 * (expose as WS)
 * @param string params (loginInfo, UpdateOrderStatusParams)
 * 
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Ezequiel Reyno
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release:
 * 			   
 */
function UpdateOrderStatus($params) {

	/* Authenticate*/

	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}
	//Auth OK
	if ($result == "true"){
		$db = JFactory::getDBO();
		$usr_id = getUserIdByUserName($params->loginInfo->login);
			
		$query = "INSERT INTO `#__virtuemart_order_histories` (`virtuemart_order_id`,`order_status_code`,";
		$query .= "`customer_notified`,`comments`,`published`,`created_on`,`created_by`,`modified_on`,`modified_by`,`locked_on`,`locked_by`)";
		$query .= "VALUES(" .$params->UpdateOrderStatusParams->order_id .",'" .$params->UpdateOrderStatusParams->status ."'," .$params->UpdateOrderStatusParams->notify .",";
		$query .= "'" .$params->UpdateOrderStatusParams->comment ."',1,'" .date( 'Y-m-d H:i:s', time()) ."'," .$usr_id .",'" .date( 'Y-m-d H:i:s', time()) ."'," .$usr_id .",'0000-00-00 00:00:00',0);";
			
		$db->setQuery($query);
		$db->query();

		$errMsg=  $db->getErrorMsg();
		if ($errMsg==null){

			$query = "";
			$query = "UPDATE `#__virtuemart_orders` SET `order_status` = '" .$params->UpdateOrderStatusParams->status ."',`modified_on` = '" .date( 'Y-m-d H:i:s', time()) ."',`modified_by` = " .$usr_id ." WHERE `virtuemart_order_id` = " .$params->UpdateOrderStatusParams->order_id .";";

			$db->setQuery($query);
			$db->query();
			$errMsg=  $db->getErrorMsg();

			if ($errMsg==null){
				return new CommonReturn("OK", "Order Updated", "");
			}else {
				return new SoapFault("GetAdditionalUserInfoFault", "Error in UpdateOrderStatus ",$errMsg);
			}
		}else {
			return new SoapFault("GetAdditionalUserInfoFault", "Error in UpdateOrderStatus ",$errMsg);
		}

	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}


/**
 * This function get all Orders
 * (expose as WS)
 * @param string params (limiteStart, LimitEnd)
 * @return array of Categories
 */
function getAllOrders($params) {
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}
	//Auth OK
	if ($result == "true"){


		if (empty($params->limite_start)){
			$params->limite_start="0";
		}
		if (empty($params->limite_end)){
			$params->limite_end="100";
		}
			
		return getOrderGeneric($params);
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}


/**
 * This function get user_info_id (copy ps_chekout.php)
 * (not expose as WS)
 * @param
 * @return array of Status
 */
function getUserInfoId($user_id){

	///////////////////TODO///////////////////
	$db = JFactory::getDBO();
	$query  = "SELECT user_info_id from `#__vm_user_info` WHERE ";
	$query .= "user_id='" . $user_id . "' ";
	$query .= "AND address_type='BT'";
	$db->setQuery($query);

	$rows = $db->loadObjectList();
	foreach ($rows as $row){
		$user_info_id=$row->user_info_id;
	}
	return $user_info_id;
	/*$db = new ps_DB();

	/* Select all the ship to information for this user id and
	* order by modification date; most recently changed to oldest
	*/
	/*$q  = "SELECT user_info_id from `#__{vm}_user_info` WHERE ";
		$q .= "user_id='" . $user_id . "' ";
		$q .= "AND address_type='BT'";
		$db->query($q);
		$db->next_record();
		return $db->f("user_info_id");*/
}

/**
 * This function get user_info_id (copy ps_chekout.php)
 * (not expose as WS)
 * @param User Name
 * @return User Id
 * 
 * @author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
 * @copyright  Ezequiel Reyno
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version    Release: 
 * 
 * */
function getUserIdByUserName($user_name){

	// Get a database object
	$db		= JFactory::getDbo();
	$query  = "SELECT id from `#__users` WHERE ";
	$query .= "username='" . $user_name . "' ";
	$db->setQuery($query);

	$rows = $db->loadObjectList();
	foreach ($rows as $row){
		$user_id=$row->id;
	}
	return $user_id;
}



/**
 * This function get all coupon code
 * (expose as WS)
 * @param string
 * @return Coupon details
 */
function GetAllCouponCode($params) {

	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		$db = JFactory::getDBO();
			
		$query  = "SELECT * FROM #__virtuemart_coupons WHERE 1 ";
			
		if (!empty($params->coupon->coupon_id)){
			$query  .= "AND virtuemart_coupon_id = '".$params->coupon->coupon_id."' ";
		}
		if (!empty($params->coupon->coupon_code)){
			$query  .= "AND coupon_code = '".$params->coupon->coupon_code."' ";
		}
		if (!empty($params->coupon->percent_or_total)){
			$query  .= "AND percent_or_total = '".$params->coupon->percent_or_total."' ";
		}
		if (!empty($params->coupon->coupon_type)){
			$query  .= "AND coupon_type = '".$params->coupon->coupon_type."' ";
		}
		if (!empty($params->coupon->coupon_value)){
			$query  .= "AND coupon_value = '".$params->coupon->coupon_value."' ";
		}
		if (!empty($params->coupon->coupon_start_date)){
			$query  .= "AND coupon_start_date > '".$params->coupon->coupon_start_date."' ";
		}
		if (!empty($params->coupon->coupon_expiry_date)){
			$query  .= "AND coupon_expiry_date < '".$params->coupon->coupon_expiry_date."' ";
		}
		if (!empty($params->coupon->coupon_value_valid)){
			$query  .= "AND coupon_value_valid = '".$params->coupon->coupon_value_valid."' ";
		}
		if (!empty($params->coupon->published)){
			$query  .= "AND published = '".$params->coupon->published."' ";
		}
			
		$db->setQuery($query);
			
		$rows = $db->loadObjectList();
			
		foreach ($rows as $row){
			$Coupon = new Coupon($row->virtuemart_coupon_id ,
					$row->coupon_code,
					$row->percent_or_total,
					$row->coupon_type,
					$row->coupon_value,
					$row->coupon_start_date,
					$row->coupon_expiry_date,
					$row->coupon_value_valid,
					$row->published
					);
			$arrayCoupon[]=$Coupon;
		}
		return $arrayCoupon;
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}



/**
 * This function get shipping rate
 * (expose as WS)
 * @param string
 * @return shipping rate
 */
function GetAllShippingRate($params) {
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->login, $params->password);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){
			
		return getNotInFreeSoap();
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->login);
	}
}



/**
 * This function get GetAllShippingMethod
 * (expose as WS)
 * @param string
 * @return shipping carrier
 */
function GetAllShippingMethod($params) {

	/* Authenticate*/
	$result = onAdminAuthenticate($params->login, $params->password);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		$db = JFactory::getDBO();
		$query  = "SELECT * FROM #__virtuemart_shipmentmethods sm ";
		$query .= "JOIN `#__virtuemart_shipmentmethods_".VMLANG."` lang ";
		$query .= "on sm.virtuemart_shipmentmethod_id = lang.virtuemart_shipmentmethod_id ";
			
		$db->setQuery($query);
			
		$rows = $db->loadObjectList();

		foreach ($rows as $row){
			//return new SoapFault("JoomlaServerAuthFault", $row->virtuemart_shipmentmethod_id);
			$shippingMethod = new ShippingMethod($row->virtuemart_shipmentmethod_id,
					$row->virtuemart_vendor_id,
					$row->shipment_jplugin_id,
					$row->slug,
					$row->shipment_element,
					$row->shipment_params,
					$row->shipment_name,
					$row->shipment_desc,
					$row->virtuemart_shoppergroup_id,
					$row->ordering,
					$row->shared,
					$row->published);
			$arrayShippingMethod[]=$shippingMethod;
		}
			
		return $arrayShippingMethod;
			
		////////////
		/*$db = new ps_DB;

		$list  = "SELECT * FROM #__{vm}_shipping_carrier WHERE 1";
		$db->query($list);
			
		while ($db->next_record()) {
			
		$ShippingCarrier = new ShippingCarrier($db->f("shipping_carrier_id"),$db->f("shipping_carrier_name"),$db->f("shipping_carrier_list_order"));
		$arrayShippingCarrier[]=$ShippingCarrier;
			
		}
		return $arrayShippingCarrier;*/
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->login);
	}
}

/**
 * This function Get All Payment Method
 * (expose as WS)
 * @param string
 * @return shipping rate
 */
function GetAllPaymentMethod($params) {
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		$query   = "SELECT * FROM #__virtuemart_paymentmethods pm ";
		$query  .= "JOIN #__virtuemart_paymentmethods_".VMLANG." lang on lang.virtuemart_paymentmethod_id=pm.virtuemart_paymentmethod_id ";
		if ($params->payment_enabled == "Y" || $params->payment_enabled == "N"){
			$query  .= "WHERE published = '$params->payment_enabled' ";
		} else {
			$query .= "WHERE 1 ";
		}
		if (!empty($params->payment_method_id)){
			$query  .= "AND pm.virtuemart_paymentmethod_id  = '$params->payment_method_id' ";
		}
			
		$db = JFactory::getDBO();
		$db->setQuery($query);
			
		$rows = $db->loadObjectList();
		//return new SoapFault("JoomlaServerAuthFault",$query);
		foreach ($rows as $row){
			$PaymentMethod = new PaymentMethod($row->virtuemart_paymentmethod_id ,
					$row->virtuemart_vendor_id ,
					$row->payment_jplugin_id ,
					$row->payment_name,
					$row->payment_element 	,
					$row->discount ,
					$row->discount_is_percentage,
					$row->discount_max_amount ,
					$row->discount_min_amount ,
					$row->payment_params,
					$row->shared ,
					$row->ordering ,
					$row->published
					);
			$arrayPaymentMethod[]=$PaymentMethod;
		}
		return $arrayPaymentMethod;
			
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}


/**
 * This function Get All Payment Method
 * (expose as WS)
 * @param string
 * @return shipping rate
 */
function GetOrderPaymentInfo($params) {
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		$order_id = $params->order_id;
			
		$db = JFactory::getDBO();
		$query  = "SELECT virtuemart_paymentmethod_id FROM #__virtuemart_orders WHERE 1 ";
			
		if (!empty($params->order_id)){
			$query  .= "AND virtuemart_order_id = '$order_id' ";
		}
		if (!empty($params->order_number)){
			$query  .= "AND order_number = '$params->order_number' ";
		}
			
		$db->setQuery($query);
			
		$rows = $db->loadObjectList();
			
		foreach ($rows as $row){
			$params->payment_method_id = $row->virtuemart_paymentmethod_id;
		}
		$pm = GetAllPaymentMethod($params);
		//return new SoapFault("JoomlaServerAuthFault",$query);
		return $pm[0];
		//return $pm;
			
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}


/**
 * This function Get Order between date
 * (expose as WS)
 * @param string
 * @return array of orders
 */
function GetOrderFromDate($params) {

	/* Authenticate*/

	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}
	//Auth OK
	if ($result == "true"){
			
		return getOrderGeneric($params);
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}

/**
 * This function get GetAllCreditCard
 * (expose as WS)
 * @param string
 * @return AllCreditCard
 */
function GetAllCreditCard($params) {

	/* Authenticate*/
	$result = onAdminAuthenticate($params->login, $params->password);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		return new SoapFault("JoomlaServerAuthFault", "No credit card in VM2");
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->login);
	}
}

/**
 * This function ChangeOrderBillTo
 * (expose as WS)
 * @param string
 * @return
 */
function GetPluginsInfo($params) {
		
	/* Authenticate*/
	$result = onAdminAuthenticate($params->loginInfo->login, $params->loginInfo->password,$params->loginInfo->isEncrypted);

	$require_auth = getConfig('COM_VM_SOA_GETORDERS');
	if ($require_auth == 0){
		$result = "true";
	}

	//Auth OK
	if ($result == "true"){

		$db = JFactory::getDBO();
		$query  = "SELECT * FROM `#__extensions` WHERE 1 ";
			
		if (!empty($params->extension_id)){
			$extension_id = $params->plugin->extension_id;
			$query .= " AND extension_id = '$extension_id' ";
		}

		if (!empty($params->plugin->name)){
			$name = $params->plugin->name;
			$query .= " AND name like '%$name%' ";
		}
			
		if (!empty($params->plugin->type)){
			$type = $params->plugin->type;
			$query .= " AND type = '$type' ";
		}
		if (!empty($params->plugin->element)){
			$element = $params->plugin->element;
			$query .= " AND element = '$element' ";
		}
		if (!empty($params->plugin->folder)){
			$folder = $params->plugin->folder;
			$query .= " AND folder like '%$folder%' ";
		}
			
			
		$query .= " ORDER BY extension_id desc ";
		//$query .= " LIMIT $params->limite_start, $params->limite_end ";
			
		$db->setQuery($query);
			
		$rows = $db->loadObjectList();
			
		foreach ($rows as $row){
			//return new SoapFault("TEST", "TEST VM_ID: ".$query."\n".$row->extension_id);
			$plugin = new Plugin($row->extension_id,
					$row->name,
					$row->type,
					$row->element,
					$row->folder,
					$row->client_id ,
					$row->enabled,
					$row->access,
					$row->protected,
					$row->manifest_cache,
					$row->params,
					$row->custom_data,
					$row->system_data,
					$row->ordering,
					$row->state
						
					);
			$pluginArray[]=$plugin;
				
		}
		return $pluginArray;
		//return new SoapFault("JoomlaServerAuthFault","Not implemented yet");
			
			
	}else if ($result == "false"){
		return new SoapFault("JoomlaServerAuthFault", "Authentication KO for : ".$params->loginInfo->login);
	}else if ($result == "no_admin"){
		return new SoapFault("JoomlaServerAuthFault", "User is not a Super Administrator : ".$params->loginInfo->login);
	}else{
		return new SoapFault("JoomlaServerAuthFault", "User does not exist : ".$params->loginInfo->login);
	}
}




/* SOAP SETTINGS */

$soap_ws_on = getConfig('COM_VM_SOA_ORDERENABLED');
$soap_ws_cache_on = getConfig('COM_VM_SOA_ORDERCACHE');

if ($soap_ws_on == 1){
		
	ini_set("soap.wsdl_cache_enabled", $soap_ws_cache_on); // wsdl cache settings
	$options = array('soap_version' => SOAP_1_2);

	/** SOAP SERVER **/
	$uri = str_replace("/free", "", JURI::root(false));
	if (empty($conf['BASESITE']) && empty($conf['URL'])){
		$server = new SoapServer('..'.DS.'VM_Orders.wsdl');
		//$server = new SoapServer($uri.'/VM_OrderWSDL.php');
	}else if (!empty($conf['BASESITE'])){
		$server = new SoapServer('http://'.$conf['URL'].'/'.$conf['BASESITE'].'/administrator/components/com_virtuemart/services/VM_OrderWSDL.php');
	}else {
		$server = new SoapServer('http://'.$conf['URL'].'/administrator/components/com_virtuemart/services/VM_OrderWSDL.php');
	}

	/* Add Functions */
	$server->addFunction("getOrdersFromStatus");
	$server->addFunction("getOrder");
	$server->addFunction("getOrderStatus");
	$server->addFunction("getAllOrders");
	$server->addFunction("GetAllCouponCode");
	$server->addFunction("GetAllShippingRate");
	$server->addFunction("GetAllShippingMethod");
	$server->addFunction("GetAllPaymentMethod");
	$server->addFunction("GetOrderFromDate");
	$server->addFunction("GetAllCreditCard");
	$server->addFunction("GetOrderPaymentInfo");
	$server->addFunction("GetPluginsInfo");
	$server->addFunction("UpdateOrderStatus");

	$server->handle();

}else{
	echoXmlMessageWSDisabled('Order');
}
?>