DROP TABLE IF EXISTS `#__vm_soap_config`;

CREATE TABLE `#__vm_soap_config` (
  `id` int(11) NOT NULL auto_increment,
  `service` varchar(128) ,
  `value` int(11) ,
  `conf` varchar(128) ,
	
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CATEGORYENABLED','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_PRODUCTENABLED','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_USERENABLED','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ORDERENABLED','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_SQLENABLED','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CUSTOMENABLED','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CATEGORYCACHE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_PRODUCTCACHE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_USERCACHE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ORDERCACHE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_SQLCACHE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CUSTOMCACHE','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_GETCATEGORIES','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ADDCATEGORY','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DELETECATEGORY','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_UPDATECATEGORY','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_GETPRODUCTS','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ADDPRODUCT','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DELETEPRODUCT','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_UPDATEPRODUCT','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_GETORDERS','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ADDORDER','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DELETEORDER','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_UPDATEORDER','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_GETUSERS','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ADDUSER','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DELETEUSER','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_UPDATEUSER','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DEFAULTGET','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DEFAULTADD','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DEFAULTUPDATE','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_DEFAULTDELETE','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CUSTOM_WS','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_CUSTOM_WS_CACHE','1');

INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ALLOW_SIMPLE_ADMIN','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('COM_VM_SOA_ALLOW_MANAGER','1');
INSERT INTO `#__vm_soap_config` (`service`,`value`) VALUES ('SOAPVAL','');
INSERT INTO `#__vm_soap_config` (`service`,`conf`) VALUES ('SOAPKEY','');
INSERT INTO `#__vm_soap_config` (`service`,`conf`) VALUES ('LASTSOAPVERSION','3.0.0');
