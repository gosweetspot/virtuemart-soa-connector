<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');

//$listOrder	= $this->escape($this->state->get('list.ordering'));
//$listDirn	= $this->escape($this->state->get('list.direction'));
$canOrder	= $user->authorise('core.edit.state', 'com_vm_soa.vm_soa');
$saveOrder	= 1;//$listOrder == 'a.ordering';

//echo $this->version;
?>

<?php 
	require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_soa'.DS.'conf.php';
	if (!FSOAP) {
/* ----------------KEY blok ---------------------- */ ?>

	
<table width="100%">
    <tr><td valign="top">
		<fieldset>
		<legend><?php echo JText::_('COM_VIRTUEMART_ADMIN_CFG_SOAP_KEY_SETTING') ?></legend>
		<table class="admintable">
	    	
	    <tr>
			<td class="key">
			  <?php echo JText::_('COM_VIRTUEMART_ADMIN_CFG_SOA_VERSION');echo '<strong>'.$this->version.'</strong>'; ?>
			</td>
		 </tr>
		 <tr>
			<td class="key">
			    <?php $lst_ver = $this->lastversion !="" ? $this->lastversion : "Unkown";
					echo JText::_('COM_VIRTUEMART_ADMIN_CFG_SOA_LATEST');echo '<strong>'.$lst_ver.'</strong>'; ?>
			</td>
		 </tr>
		 <tr>
			<td class="key" >
				<span class="hasTip" title="<?php echo JText::_('COM_VIRTUEMART_ADMIN_CFG_SOAP_KEY'); ?>">
				<?php echo JText::_('COM_VIRTUEMART_ADMIN_CFG_SOAP_KEY') ?>
				</span>
			</td>
				<td>
				<input type="text" name="soapkey" class="inputbox" value="<?php echo $this->soapkey ?>" />
			</td>
		</tr>
		<tr>
			<td class="key">
			<?php 	$soap_val =  $this->soap_val;
					$uri = JURI::base();
					/*echo $uri;
					$uri = str_replace('administrator/components/com_vm_soa/services/', "", $uri);*/
					$btn_ok = $uri.'components/com_vm_soa/images/button_ok.png';
					$btn_ko = $uri.'components/com_vm_soa/images/error_button.gif';		  
			
					if ($soap_val==1){
				  	?>
				 		 <img alt="Key valid" src="<?php echo $btn_ok ?>"   > 
			<?php } else { ?>
						<img alt="Key not valid" src="<?php echo $btn_ko ?>"   >
			<?php }  ?>
			
			</td>
		</tr>
	
	    </table>
	    </fieldset>
	</td></tr>
</table>

<?php } //end fsoap ?>
<table class="table table-striped">
	
		<tr>		
			<th>
				<a href="components/com_vm_soa/services/VM_CategoriesWSDL.php">Categories WSDL</a>
			</th>
			<th>
				<a href="components/com_vm_soa/services/VM_SQLQueriesWSDL.php">SQL WSDL</a>
			</th>
			<th>
				<a href="components/com_vm_soa/services/VM_OrderWSDL.php">Orders WSDL</a>
			</th>
			<th>
				<a href="components/com_vm_soa/services/VM_ProductWSDL.php">Products WSDL</a>
			</th>
			<th>
				<a href="components/com_vm_soa/services/VM_UsersWSDL.php">Users WSDL</a>
			</th>
		</tr>
</table>

<form action="index.php" method="post" name="adminForm">
<div id="editcell">
	<table class="table table-striped">
	<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( 'ID' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>	
			<th width="40">
				<?php echo JText::_( 'Active' ); ?>
			</th>			
			<th>
				<?php echo JText::_( 'Services' ); ?>
			</th>
			
			
		</tr>
	</thead>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)	{
		
		$row = &$this->items[$i];
		if  ($row->service == "SOAPVAL" || $row->service == "SOAPKEY" || $row->service == "LASTSOAPVERSION" ){
			break;
		}
		
		$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $row->checked_out == $userId || $row->checked_out == 0;
		$canChange	= $user->authorise('core.edit.state',	'com_redsoap.'.$row->id) && $canCheckin;
		
		$checked 	= JHTML::_('grid.id',   $i, $row->id );
		$link 		= JRoute::_( 'index.php?option=com_vm_soa&controller=vm_soa&task=edit&cid[]='. $row->id );
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $row->id; ?>
			</td>
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
				<?php echo JHtml::_('jgrid.published',$row->value,  $i, '', true, 'cb', '2010-04-25 14:45:09','2020-04-25 14:45:09'); ?>
			</td>
			<td>
				<a href="<?php echo $link; ?>" title="<?php echo JText::_( $row->service.'_TIP' ); ?>">
					<?php echo JText::_( $row->service ); ?>
				
				</a>
			</td>
			
			
			
			
		</tr>
		
		<?php
		$k = 1 - $k;
	}
	?>
	</table>
</div>

<br>

<?php /* ----------------LINK BLOK ---------------------- */ ?>
<table width="100%">
    <tr><td valign="top">
		<fieldset>
		<legend><?php echo JText::_('COM_VM_SOA_ADMIN_LINK_HEAD') ?></legend>
		<table class="admintable">
	
	    	<?php 
			 echo "<ul>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/' TARGET='_blank' >". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_WEB')." </a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_content&view=category&id=27:new-to-joomla&layout=blog&Itemid=44&layout=default' TARGET='_blank' >". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_WSDOC')." </a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_content&view=category&id=27:new-to-joomla&layout=blog&Itemid=44&layout=default' TARGET='_blank' > ". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_FAQ')."</a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_kunena&Itemid=62' TARGET='_blank' > ". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_FORUM')." </a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_virtuemart&page=shop.browse&category_id=18&Itemid=29&vmcchk=1&Itemid=29' TARGET='_blank' > ". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_STORE')." </a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_contact&view=contact&id=2&Itemid=60' TARGET='_blank' > ". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_CONTACT')." </a></li>";
			 echo "<li><a href='http://www.virtuemart-datamanager.com/index.php?option=com_content&view=article&id=71:how-to-test-webservice&catid=27:new-to-joomla&Itemid=44' TARGET='_blank' > ". JText::_('COM_VM_SOA_ADMIN_CFG_LINK_HOWTOTEST')." </a></li>";
			 echo "<li></li>";
			 echo "<li><a href='components/com_vm_soa/services/VM_CategoriesWSDL.php'>Categories WSDL</a></li>";
			 echo "<li><a href='components/com_vm_soa/services/VM_SQLQueriesWSDL.php'>SQL WSDL</a></li>";
			 echo "<li><a href='components/com_vm_soa/services/VM_OrderWSDL.php'>Orders WSDL</a></li>";
			 echo "<li><a href='components/com_vm_soa/services/VM_ProductWSDL.php'>Products WSDL</a></li>";
			 echo "<li><a href='components/com_vm_soa/services/VM_UsersWSDL.php'>Users WSDL</a></li>";
			 echo "</ul>";
			
			?>

	    </table>
	    </fieldset>
	</td></tr>
</table>

<input type="hidden" name="option" value="com_vm_soa" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="vm_soa" />
</form>
