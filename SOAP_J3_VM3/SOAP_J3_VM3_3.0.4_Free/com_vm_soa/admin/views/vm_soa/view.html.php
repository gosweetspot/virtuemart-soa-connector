<?php

/**
 * @package    	com_vm_soa (WebServices for virtuemart 2)
 * @author		Mickael Cabanas (cabanas.mickael|at|gmail.com)
 * @link 		http://www.virtuemart-datamanager.com
 * @license    	GNU/GPL
*/


defined('_JEXEC') or die();
require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_vm_soa'.DS.'conf.php';
	
jimport( 'joomla.application.component.view' );

class vm_soaViewvm_soa extends JViewLegacy{
	
  function config($tpl = null){
	
  }
  
  function display($tpl = null){
  	
	$this->loadVM();
	
	$version = $this->getVersion();
	$soapkey = $this->getSoapKey();
	
	$lastversion = $this->getLastSoapVersion();
	
	
	$data = $this->getSoap_val($soapkey);
	$this->saveSoapVal($data['soap_val']);
	$this->saveLastVersion($data['last_version']);
	$soap_val = $data['soap_val'];
	
	$task = JRequest::getCmd( 'task' );
	$act = JRequest::getCmd( 'act' );

 	// Set the toolbar
	$this->addToolBar();
 
    //$items = $this->get( 'Data');
	//$items		= & $this->get( 'Data');
	$items = $this->getData();
    $this->assignRef('items', $items);
	
	
	$this->assignRef('version',$version);
	$this->assignRef('soapkey',$soapkey);
	$this->assignRef('lastversion',$lastversion);
	$this->assignRef('soap_val',$soap_val);
    parent::display($tpl);
	
  }
  
	protected function addToolBar() 
	{
		JToolBarHelper::title( JText::_( 'COM_VM_SOA_ADMIN_VIEW_NAME' ),  'head vm_config_48' );
		JToolBarHelper::divider();
        JToolBarHelper::save();
        JToolBarHelper::apply();
        JToolBarHelper::cancel();
		
		JToolBarHelper::unpublishList();
		JToolBarHelper::publishList();
        
	}
	
	function loadVM(){
  	
		if(!class_exists('AdminUIHelper'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'adminui.php');
		if(!class_exists('ShopFunctions'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'shopfunctions.php');
		if(!class_exists('VmHTML'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'html.php');
		if(!class_exists('VmConfig'))require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');
			
		
		$config = VmConfig::loadConfig();
		$this->assignRef('config', $config);
		
		$this->loadHelper('adminui');
		$this->loadHelper('image');
		$this->loadHelper('html');
		$this->loadHelper('shopFunctions');
		
		$front = JURI::root(true).'/components/com_virtuemart/assets/';
		$admin = JURI::base().'components/com_virtuemart/assets/';	
		$document = JFactory::getDocument();

		//loading defaut admin CSS
		$document->addStyleSheet($admin.'css/admin_ui.css');
		$document->addStyleSheet($admin.'css/admin_menu.css');
		$document->addStyleSheet($admin.'css/admin.styles.css');
		$document->addStyleSheet($admin.'css/toolbar_images.css');
		$document->addStyleSheet($admin.'css/menu_images.css');
		$document->addStyleSheet($front.'css/chosen.css');
		$document->addStyleSheet($front.'css/vtip.css');
		$document->addStyleSheet($front.'js/fancybox/jquery.fancybox-1.3.4.css');
		//$document->addStyleSheet($admin.'css/jqtransform.css');

		//loading defaut script

		$document->addScript($front.'js/fancybox/jquery.mousewheel-3.0.4.pack.js');
		$document->addScript($front.'js/fancybox/jquery.easing-1.3.pack.js');
		$document->addScript($front.'js/fancybox/jquery.fancybox-1.3.4.pack.js');
		$document->addScript($admin.'js/jquery.cookie.js');
		$document->addScript($front.'js/chosen.jquery.min.js');
		$document->addScript($admin.'js/vm2admin.js');
		
		$document->addScriptDeclaration ( '
			var tip_image="'.JURI::root(true).'/components/com_virtuemart/assets/js/images/vtip_arrow.png";
			');
  	
  	
	}
	
	/**
	 * Hellos data array
	 *
	 * @var array
	 */
	var $_data;
	
	/**
	 * Returns the query
	 * @return string The query to be used to retrieve the rows from the database
	 */
	/*function _buildQuery()
	{
		$query = ' SELECT * '
			. ' FROM #__vm_soap_config'
		;

		return $query;
	}*/

	/**
	 * Retrieves the hello data
	 * @return array Array of objects containing the data from the database
	 */
	function getData()
	{
		$db = JFactory::getDBO();
				
		$query = ' SELECT * '. ' FROM #__vm_soap_config';
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		foreach ($rows as $row)	{
		}
		

		return $rows;
	}
	
	function getVersion(){
			$version ="";
			$_VERSION = new JVersion();
			
			if ($_VERSION->RELEASE == "1.5"){
				$version ="2.0.0";
			}else{
				
				$db = JFactory::getDBO();
				
				$query  = "SELECT manifest_cache FROM `#__extensions` ext ";
				$query .= "WHERE element ='com_vm_soa'  ";
				
				$db->setQuery($query);
				$rows = $db->loadObjectList();
				
				foreach ($rows as $row)	{

					$data = explode(',', $row->manifest_cache);
					$count = count($data);
					for ($i = 0; $i < $count; $i++) {
						//echo "data ".$data[$i];
						$data2 = explode(':',$data[$i]);
						//echo "data :".$data2[0].' -> '.$data2[1].'\n';
						if ($data2[0]=='"version"'){
						
							$version = $data2[1];
						}
					}
				
				}
				$version = str_replace('"', '', $version);
			
			}
			
			
			return $version;

	}
	
	function getSoapKey(){
			
				
		$db = JFactory::getDBO();
				
		$query = " SELECT *  FROM #__vm_soap_config where service = 'SOAPKEY' ";
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		foreach ($rows as $row)	{
			$key = $row->conf;
		}
		

		return $key;

	}
	
	function getLastSoapVersion(){
			
				
		$db = JFactory::getDBO();
				
		$query = " SELECT *  FROM #__vm_soap_config where service = 'LASTSOAPVERSION' ";
		
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		foreach ($rows as $row)	{
			$key = $row->conf;
		}
		

		return $key;

	}
	function getSoap_val($key){
		if (!FSOAP) {
			$data= $this->getValue($key);
			return $data;
		} else {return "1";
		}
	}
	
	function saveSoapVal($val){
		$dbInsert = JFactory::getDBO();	
		$query   = "UPDATE `#__vm_soap_config` ";
		$query  .= "SET value = '$val' ";
		$query  .= "WHERE service = 'SOAPVAL' ";
		$dbInsert->setQuery($query);
		$dbInsert->query();
				
		$errMsg=  $dbInsert->getErrorMsg();
	}
	
	function saveLastVersion($val){
		$dbInsert = JFactory::getDBO();	
		$query   = "UPDATE `#__vm_soap_config` ";
		$query  .= "SET conf = '$val' ";
		$query  .= "WHERE service = 'LASTSOAPVERSION' ";
		$dbInsert->setQuery($query);
		$dbInsert->query();
				
		$errMsg=  $dbInsert->getErrorMsg();
	}
	
	function loadConf()
	{
		include('settings.php');		
	}
	
	function getValue($soapkey){ 
		
		$data['soap_val']=0;
		$data['last_version']='Unknown';
		$data['msg']='Unknown';
		
		$error = 0;
		
		$url_wsdl = 'http://www.virtuemart-datamanager.com/administrator/modules/mod_valid_soa3/services/VM_ValidWSDL.php';
		//$url_wsdl = 'E:\Dev\wamp\www\J30\administrator\components\com_vm_soa\views\vm_soa\VM_Valid.wsdl';
	
		ini_set("soap.wsdl_cache_enabled", 0); // wsdl cache settings
		try {
           $soapClient = new SoapClient($url_wsdl);
        } catch (Exception $e) {
			$data['soap_val']=1;
			$data['msg'] = "Warning: Unable to connect to licence server";
			$error = 0;
			return $data;

		}
	
		$key = $soapkey;
        $ap_param = array(
                    'host'     =>   $_SERVER['SERVER_NAME'],
					'key'     =>    $key,
					'params2' =>   $_SERVER['SERVER_ADDR']
					);
                   
       
        
        try {
            $resp = $soapClient->__call("Valid", array($ap_param));
        } catch (SoapFault $fault) {
            $error = 1;
			$data['soap_val']=0;
			$data['msg'] = "Error : Error on licence server";
			return $data;;
			
        }
       
        if ($error == 0) {  
			$lst = $resp->returnData;
			$data['last_version'] = $lst;
			
        	if ($resp->returnCode){
        		$data['soap_val']="1";
        	}

        }
		
		return $data;
	}
}
?>
		
