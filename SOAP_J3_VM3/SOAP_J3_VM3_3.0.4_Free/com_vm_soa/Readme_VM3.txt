/**
 * com_vm_soa
 */
 
package    com_vm_soa
author     Mickael cabanas (cabanas.mickael|at|gmail.com)
author	   Ezequiel Reyno (ezequiel.reyno|at|gmail.com)
copyright  2015 Mickael Cabanas
license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
website	   http://www.virtuemart-datamanager.com
version    3.0.5

com_vm_soa component for Joomla 3.x - Virtuemart 3.x (need PHP5.2 or highter and soap module enabled)
You can access webservices with many langages like PHP - Java - Flash/Flex - C++ - Javascript - VB ....
Test your webservices with tool like SOAPui ! 


Release info:

3.0.5

 Support function UpdateOrderStatus.
 Include more properties to the Order object.

3.0.0

 Support for Joomla 3.X and VM3 (2.9.9b)

2.0.20
 fix update user

2.0.19
 update for vm 2.6
 product mpn/gtin

2.0.18
 Fix add/update prod (prices)
 
2.0.17

- Fix Admin authenticate problem for Joomla 2.5.18 user 
- Now strong encryption is supported
- Fix addProduct (double add)

2.0.16
- Fix missing usergroups model in VM 2.0.26d
- Change GetAuthGroup (based on permission.php)
- Add and Update auth Group is now a sql request

2.0.15
- Fix update product

2.0.14
- Fix update product

2.0.13

- Add customtitle on category/product
- Add shopper groupids on product
- Add getTemplates
- Fix update orderstatus (send mail)
- Fix Add manufacturer return last id
- Fix add manufacturer with URL
- add address_type on GetAdditionalUserInfo
- rework add/upadte shipping adress

2.0.12

- Tested on vm2.0.16
- Fix Add/update prices
- Fix add prod / update prod (manage multi prices)
- AddCustomField (check relation)

2.0.11

- Tested on vm2.0.14
- Add GetPluginsInfo
- Fix searchProduct (child)
- ADD Method ChangeOrderPayment (VM_Orders)
- ADD Method ChangeProductItemPrice (VM_Orders)
- ADD Method ChangeItemQuantity (VM_Orders)
- ADD Method AddOrderItem (VM_Orders)
- ADD Method DeleteOrderItem (VM_Orders)
- ADD Method ChangeOrderCouponDiscount (VM_Orders)
- ADD Method ChangeOrderShipping (VM_Orders)
- ADD Method ChangeOrderCustomerNote (VM_Orders)
- ADD Method ChangeOrderShipTo (VM_Orders)
- ADD Method ChangeOrderBillTo (VM_Orders)
(all not implemented, waiting vm team )

2.0.10
- Add custom list
- Delete custom list
- Update custom list
- Fix limit in getProdFromCat
- Fix auth method (allow multi groups)
- change checkVersion

2.0.9
- Fix SOA BE config with vm2.0.7

2.0.8
- Fix deleteMedia product/manfacturer/vendor

2.0.7
- Fix create order session
- Fix create order dir separator
- Fix updateuser
- fix getPrices for VM2.0.7rc

2.0.6
- ShippingAdress (add up del) beta
- Finalyse createOrder
- add product_final_price and product_price_info to product_price (VM calculated prices)
- fix publish review
- add extra_fields_data to user (for extra userfields)

2.0.5
- fix searchProduct
- add img_thumb_uri to manufacrturer and vendor
- addManufacturer add default media
- addMediaManufacturer works with file_url/file_url_thumb
- Add GetWorldZones,AddWorldZones,UpdateWorldZones,DeleteWorldZones
- Virtuemart 2.0.6 compatibility

2.0.4
- VM core limit max products to 700. limit is now removed
- custom_param added to CustomField
- Add new WS : AddCustomField,DeleteCustomField,UpdateCustomField
- Virtuemart 2.0.4 compatibility

2.0.3
- Link media (add existing media to a product or category ... )

2.0.2
- add media when add Category
- remove limite 10 on getManufacturer
- update to vm 2.0.3
- fix getMedia pb
- add fileThumbPath to addMediaInput

2.0.1
- Joomla 2.5 compatibility
- Virtuemart 2.0.2 compatibility
- add ordering/shared to wsdl (product and cat)
- Fix bug on update (ordering)

2.0.0

- J1.7 and VM 2.0.0

2.0.0-RC

- Test on VM 2.0.0

2.0.0-Beta1
Big changes
Work with VM2(rc2) and Joomla 1.6 (not tested with j1.7)
Keep same wsdl as possible
Some fields name changed, added or deleted
Now possible to pass binary data to SOAP request (binary64) for adding media



1.5.15
- add ChangeProductItemPrice
- add ChangeOrderPayment
- add ChangeItemQuantity
- add AddOrderItem
- add DeleteOrderItem
- add ChangeOrderCouponDiscount
- add ChangeOrderShipping
- add ChangeOrderCustomerNote
- add ChangeOrderShipTo
- add ChangeOrderBillTo
- add AddShippingAdress
- add UpdateShippingAdress
- add DeleteShippingAdress
- add order_item_id element to GetProductFromOrderId (used for ChangeOrder... methods )


1.5.14
- Add limite start limite end to getAvailableImages
- Fix bug on update product (bug on related product)
- fix bug on Search product when manufacturer is not set

1.5.13
- Product : Change atribute and atribute_value. Now all is returned in atribute_value. ex <atribute_value>Color:blue|Capacity:800 GB</atribute_value>
- Add or update Products : Now child product attributes is supported : Format <atribute_value>Color:blue|Capacity:800 GB</atribute_value>
- Note : Attribute name must exists in parent
- Add GetProductAttributeList

1.5.12
- Allow Manager (Joomla rights) to use SOAP
- Change return of method Authentification. Return Joomla Type, VM perms or SOAP fault if auth ko 

1.5.11
- correc protect wsdl buffer

1.5.10
- protect WSDL buffer
- Fix bug in wsdl
- Allow Superadmin and Admin to connect

1.5.9
add GetOrderPaymentInfo
add NotifyWaintingList
add GetProductVote
add GetProductReviews
add PublishReviews
add GetUserInfoFromOrderID
add GetRelatedProducts
add SetRelatedProducts

1.5.8
add getSessions
add getWaintingList

1.5.7
add getproductFromSKU
add searchProducts ( operator = AND |OR , operator_more_less_equal = moree|less|equal|moreequal|lessequal)
Optimisation GetAllProduct (sql optimisation)
modify addproduct/updateProduct (childoption quantity options)order_levels, included_product_id fix bug
modify date getorder
sendEmail add cc cci images attachment replyto 
GetAvalaibleImage add  realpath

1.5.6

- Administration change : URL (Hostname and base configuration is get automatically)
- todo fix bug euro symbol in update vendor
- todo implement new filed in adduser updateuser getuser
- todo payment method (enable/disable/forumulaire eg paypal)

1.5.5

- add GetDiscount
- add AddDiscount
- add UpdateDiscount
- add DeleteDiscount
- add UpdateProductDiscount
- add GetProductFile
- add AddProductFile
- add UpdateProductFile
- add DeleteProductFile
- Modifiy VM_Upload.php (add params thumbW,thumbH for thumb creation, DIR = media to upload to media directory)
- Change GetAvailableFile (add img_type : values : all, thumb, full)

1.5.4

- add GetProductPrice
- add AddProductPrice
- add UpdateProductPrice
- add DeleteProductPrice
- Fix SQL Query return error code
- Fix getOrdersFromStatus when limit empty (default 0-100)
- getOrdersFromStatus (add sql "order by order id desc")

1.5.3

- Fix wsdl HTTP header 'Content-type: text/xml; charset=UTF-8', and lenght
- add customer_number, groupe_id (gid) 
user_info_id,address_type,address_type_name,company,middle_name,
phone_2,extra_field_1,extra_field_2,extra_field_3,extra_field_4,extrtra_field_5 
in addUser, updateuser
- add shopper_group_id in product wsdl (VM_ProductService.php) for addProduct updateProduct
- Fix bug when base url is empty
- fix bug when product price is 0
- add <product_publish> <with_childs> to GetProductsFromCategory
- fix bug in addCategory when image is not null and is not URL
- fix bug in addProduct when image is not null and is not URL
- add publishProduct and category

1.5.2

- Fixed bug in CreateOrder (rename fields and add vendor_id)
- Fixed bug in DeleteVendor
- Fixed bug in UpdateVendor
- Fixed bug in DeleteOrder
- Fixed bug in UpdateCategorie ,img not removed when <image> is null
- Fixed bugs in AddCategorie - productperrow,
- Fixed bugs in UpdateCategorie - productperrow,
- Fixed bugs in UpdateProduct -   img not removed when <image> is null
- Fixed bugs in AddProduct -  
- Add Bank acount info to Customer
- Add param category_publish in GetAllCategories and GetChildsCategories
- Add GetVersion
- Add VM_Upload
- fix AddCreditCard, update , (add vendor id)
- fix bug AddManufacturer, UpdateManufacturer (category_id)
- fix UpdatePaymentMethod , AddPaymentMethod (shopperGroup_id)
- fix UpdateTax, Add Tax (ps_vendor) (NOT WORK)
- fix vendor_id for UpdateShopperGroup , AddShopperGroup
- add UpdateShippingCarrier
- add UpdateShippingRate
- add search by user_ids into GetUserFromEmailOrUsername
- GetOrderFromDate correct end date
- fix bug DeleteShippingRate
- Modify GetUserFromEmailOrUsername , add searh by name/lastname
- Modify GetUsers ,  limite start  end
- modify AddProduct (add manufacturer_id, vendor_id)

RC_1.5.1

- add product_sales to product object
- Correction Limite GetAllProduct
- Change SQL Return 
- Correct Getusers(add username)
- Change name gettAllCouponCode to getAllCouponCode
- Add GetAllCreditCard
- Add AddCreditCard
- Add UpdateCreditCard
- Add DeleteCreditCard
- Add UpdateUser
- Add AddOrderStatusCode
- Add UpdateOrderStatusCode
- Add DeleteOrderStatusCode
- Modify UdpatusOrder (massUpdate)
- Modify getOrderStatus 
- Add GetAllVendor
- Add AddVendor
- Add UpdateVendor
- Add DeleteVendor
- Add GetAllVendorCategory
- Add AddVendorCategory
- Add UpdateVendorCategory
- Add DeleteVendorCategory
- Add GetAllManufacturer
- Add AddManufacturer
- Add UpdateManufacturer
- Add DeleteManufacturer
- Add GetAllManufacturerCat
- Add AddManufacturerCat
- Add UpdateManufacturerCat
- Add DeleteManufacturerCat
- Add GetAvailableVendorImages

RC_1.5

- Fix bug delete image when updateproduct
- Change Date Format in getAllOrders
- Add GetAllCurrency
- Add GetAllCountryCode
- Add GettAllCouponCode
- Add AddCouponCode
- Add DeleteCouponCode
- Add GetAllShippingRate
- Add GetAllShippingCarrier
- Add AddShippingRate
- Add AddShippingCarrier
- Add DeleteShippingCarrier
- Add DeleteShippingRate
- Add GetAllPaymentMethod
- Add AddPaymentMethod
- Add UpdatePaymentMethod
- Add DeletePaymentMethod
- Add GetAuthGroup
- Add AddAuthGroup
- Add DeleteAuthGroup
- Add getAllStates
- Add addStates
- Add deleteStates
- Add GetAllTax
- Add AddTax
- Add UpdateTax
- Add DeleteTax
- Add GetShopperGroup
- Add AddShopperGroup
- Add UpdateShopperGroup
- Add DeleteShopperGroup
- Add GetAllProducts
- Add GetAvailableImages (products)
- Add GetAvailableImages (categories)
- Add UpdateCategory
- Add GetOrderFromDate

RC1.0.4

- Fix bug encoding CategoriesService
- Add CreateOrder function (beta)
- Add search user function (by username/email)

RC1.0.3

- New architecure
- New Back office Configuration  
- Enable/Disable Authentification per service
- Add Checkbox
 
RC1.0.2

- Add Send Mail function
- Mail notification correction

RC1.0.1

- fix bug in addProduct and updateProduct
- Add All fields in product 

RC1.0

- Implement SQLQueriesService
- Add Authentification in all services (Only super admin can you webservice)
- AddUser
- DeleteUser
- GetProductsFromOrderId
- DeleteOrder
- UpdateOrder
- AddCategory
- DeleteCategory
- AddProduct
- UpdateProduct
- DeleteProduct

1- DESCRIPTION

This component provide Webservices for virtuemart :

	- VM_Categories
	- VM_Orders
	- VM_Product
	- VM_SQLQueries
	- VM_Users
	- More in next release


2- INSTALLATION

Go to joomla admin interface -> Extensions -> install -> Upload Package File and select zip file from your desk (click upload & install)

3- CONFIGURATION

Just install it.
Components -> WebService Virtuemart -> Configuration
Fill hostname of the server
Fill base of joomla install
Do not modify file name (wsdl and php) if you do not change file name on the server before.
To enable/disable webservice tick checkbox.
Click save to save the config.

4- WEBSERVICE ACCESS

	- VM_Categories
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_CategoriesWSDL.php

	- VM_Orders
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_OrderWSDL.php

	- VM_Product
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_ProductWSDL.php

	- VM_SQLQueries
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_SQLQueriesWSDL.php
	
	- VM_Users
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_UsersWSDL.php
	
	- VM_Upload (not a soap webservice)
	http://<host>/<base>/administrator/components/com_vm_soa/services/VM_Upload.php


5- TEST WEBSERVICE

PHP Sample code : 

	$client = new SoapClient("http://<host>/<base>/administrator/components/com_vm_soa/services/VM_CategoriesWSDL.php");
	$catarray = array($client->GetChildsCategories($catid));

You can test all webservices with SOAPui for example. 

Some SOAP Request sample:

	- VM_Categories (GetAllCategories method)

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Categories/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:GetAllCategoriesRequest>
         <!--You may enter the following 2 items in any order-->
         <login> superAdmin login </login>
         <password> superAdmin pass </password>
      </vm:GetAllCategoriesRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Categories (GetChildsCategories method)
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Categories/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:GetChildsCategoriesRequest>
         <!--You may enter the following 2 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
         </loginInfo>
         <categoryId>0</categoryId>
      </vm:GetChildsCategoriesRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (getAllOrders method)
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:getAllOrdersRequest>
         <!--You may enter the following 3 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
         </loginInfo>
         <limite_start>0</limite_start>
         <limite_end>100</limite_end>
      </vm:getAllOrdersRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (getOrder method)
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:getOrderRequest>
         <!--You may enter the following 4 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
         </loginInfo>
         <order_id>1</order_id>
         <limite_start>0</limite_start>
         <limite_end>100</limite_end>
      </vm:getOrderRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (getOrdersFromStatus method)
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:getOrdersFromStatusRequest>
         <!--You may enter the following 4 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
         </loginInfo>
         <limite_start>0</limite_start>
         <limite_end>100</limite_end>
         <status>P</status>
      </vm:getOrdersFromStatusRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (getOrdersStatus method)

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:getOrderStatusRequest>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
      </vm:getOrderStatusRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_SQLQueries (ExecuteSQLSelectQuery method)

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_SQLQueries/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:ExecuteSQLSelectQueryRequest>
         <!--You may enter the following 4 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
			<login> superAdmin login </login>
			<password> superAdmin pass </password>
         </loginInfo>
		 <columns>
            <!--Zero or more repetitions:-->
            <column>product_id</column>
            <column>product_name</column>
            <column>product_full_image</column>
         </columns>
         <table>#__{vm}_product</table>
         <whereClause>WHERE 1 </whereClause>
      </vm:ExecuteSQLSelectQueryRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_SQLQueries (ExecuteSQLInsertQuery method) : This an SQL insert new orderStatus code in table

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_SQLQueries/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:ExecuteSQLInsertQueryRequest>
         <!--You may enter the following 5 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>*******</password>
         </loginInfo>
         <table>#__{vm}_order_status</table>
         <whereClause></whereClause>
         <columns>
            <!--Zero or more repetitions:-->
            <column>order_status_code</column>
            <column>order_status_name</column>
         </columns>
         <values>
            <!--Zero or more repetitions:-->
            <value>D</value>
			<value>Downloaded</value>
         </values>
      </vm:ExecuteSQLInsertQueryRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_SQLQueries (ExecuteSQLUpdateQuery method) : This an SQL update querie of order Status code (P to R)
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_SQLQueries/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:ExecuteSQLUdpateQueryRequest>
         <!--You may enter the following 5 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>*****</password>
         </loginInfo>
         <table>#__{vm}_order_status</table>
         <whereClause>WHERE order_status_code='P'</whereClause>
         <columns>
            <column>order_status_code</column>
            <column>vendor_id</column>
         </columns>
         <values>
            <value>R</value>
			<value>1</value>
         </values>
      </vm:ExecuteSQLUdpateQueryRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_SQLQueries (ExecuteSQLQuery method) : This is a generic SQL request

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_SQLQueries/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:ExecuteSQLQueryRequest>
         <!--You may enter the following 2 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>*******</password>
         </loginInfo>
         <sqlRequest>select * from #__{vm}_orders WHERE 1</sqlRequest>
      </vm:ExecuteSQLQueryRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Users (SendMail method) 
	
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Users/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:sendMailRequest>
         <loginInfo>
            <login>admin</login>
            <password>*******</password>
         </loginInfo>
         <EmailParams>
            <from_mail>email@email.com</from_mail>
            <from_name>name</from_name>
            <recipient>email@email.com</recipient>
            <subject>test webservice</subject>
            <body><![CDATA[<p> you can add html code inside CDATA </p>]]></body>
            <altbody></altbody>
         </EmailParams>
      </vm:sendMailRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (create order method) 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:CreateOrderRequest>
         <loginInfo>
            <login>admin</login>
            <password>******</password>
         </loginInfo>
         <user_id>63</user_id>
         <shipping_method>standard_shipping</shipping_method>
         <shipping_carrier_name>UPS</shipping_carrier_name>
         <shipping_rate_name>World</shipping_rate_name>
         <shipping_price>5.9</shipping_price>
         <shipping_rate_id>22</shipping_rate_id>
         <price_including_tax></price_including_tax>
         <product_currency>EUR</product_currency>
         <customer_note>THe note</customer_note>
         <payment_method_id>4</payment_method_id>
         <coupon_code></coupon_code>
         <products>
            <!--Zero or more repetitions:-->
            <product>
               <product_id>21</product_id>
               <description>desc</description>
               <quantity>1</quantity>
            </product>
            <product>
               <product_id>22</product_id>
               <description>desc</description>
               <quantity>2</quantity>
            </product>
         </products>
      </vm:CreateOrderRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (getUser From Email Or Username method) 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Users/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:getUserFromEmailOrUsernameRequest>
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>*******</password>
         </loginInfo>
         <email>mail@mail.com</email>
         <username>theUsername</username>
         <searchtype>email</searchtype>
      </vm:getUserFromEmailOrUsernameRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (add Coupon) 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:AddCouponCodeRequest>
         <!--You may enter the following 2 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>******</password>
         </loginInfo>
         <coupons>
            <!--Zero or more repetitions:-->
            <coupon>
               <!--You may enter the following 5 items in any order-->
               <coupon_id></coupon_id>
               <coupon_code>REDUC_A</coupon_code>
               <percent_or_total>total</percent_or_total>
               <coupon_type>gift</coupon_type>
               <coupon_value>10</coupon_value>
            </coupon>
            <coupon>
               <!--You may enter the following 5 items in any order-->
               <coupon_id></coupon_id>
               <coupon_code>REDUC_B</coupon_code>
               <percent_or_total>percent</percent_or_total>
               <coupon_type>permanent</coupon_type>
               <coupon_value>10</coupon_value>
            </coupon>
         </coupons>
      </vm:AddCouponCodeRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (add payment method) 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:AddPaymentMethodRequest>
         <!--You may enter the following 2 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>*****</password>
         </loginInfo>
         <payment_method>
            <payment_method_id></payment_method_id>
            <vendor_id>1</vendor_id>
            <payment_method_name>Paypal</payment_method_name>
            <payment_class/>
            <shopper_group_id>6</shopper_group_id>
            <payment_method_discount>0.00</payment_method_discount>
            <payment_method_discount_is_percent>0</payment_method_discount_is_percent>
            <payment_method_discount_max_amount>0.00</payment_method_discount_max_amount>
            <payment_method_discount_min_amount>0.00</payment_method_discount_min_amount>
            <list_order>4</list_order>
            <payment_method_code>Ppl</payment_method_code>
            <enable_processor>N</enable_processor>
            <is_creditcard>0</is_creditcard>
            <payment_enabled>Y</payment_enabled>
            <accepted_creditcards/>
            <payment_extrainfo/>
         </payment_method>
      </vm:AddPaymentMethodRequest>
   </soapenv:Body>
</soapenv:Envelope>

	- VM_Orders (get order form date) 

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:vm="http://www.virtuemart.net/VM_Order/">
   <soapenv:Header/>
   <soapenv:Body>
      <vm:GetOrderFromDateRequest>
         <!--You may enter the following 4 items in any order-->
         <loginInfo>
            <!--You may enter the following 2 items in any order-->
            <login>admin</login>
            <password>******</password>
         </loginInfo>
         <order_status>C</order_status>
         <date_start>2010-01-30</date_start>
         <date_end>2010-06-30</date_end>
      </vm:GetOrderFromDateRequest>
   </soapenv:Body>
</soapenv:Envelope>
...

