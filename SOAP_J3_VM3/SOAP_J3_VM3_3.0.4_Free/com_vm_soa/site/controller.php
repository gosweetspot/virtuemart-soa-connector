<?php
/**
 * @package    	com_vm_soa (WebServices for virtuemart)
 * @author		Mickael Cabanas (cabanas.mickael|at|gmail.com)
 * @link 		http://www.virtuemart-datamanager.com
 * @license    	GNU/GPL
*/
 //var_dump(" die;bitch"); die;
// No direct access
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.application.component.controller');
 
/**
 * Hello World Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class vm_soaController extends JControllerLegacy
{

	/*$jversion = new JVersion();
	if( version_compare( $jversion->getShortVersion(), '3.1', 'ge' ) )
	{
	   // Joomla 3.x code.
	} else {
	   // Joomla 2.5 code
	}*/
	function __construct() {
		parent::__construct();
		/*if (VmConfig::get('shop_is_offline') == '1') {
		    vRequest::setVar( 'layout', 'off_line' );
	    }
	    else {
		    vRequest::setVar( 'layout', 'default' );
	    }*/
	}

	/**
	 * Override of display to prevent caching
	 *
	 * @return  JController  A JController object to support chaining.
	 */
	public function display($cachable = false, $urlparams = false){

		$document = JFactory::getDocument();
		$viewType = $document->getType();
		$viewName = "vm_soa";
		$viewLayout = "default";

		//vmdebug('basePath is NOT JPATH_VM_SITE',$this->basePath,JPATH_VM_SITE);
		$view = $this->getView($viewName, $viewType);
		$view->assignRef('document', $document);

		$view->display();

		return $this;
	}



}
 //pure php no closing tag
